<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/search/{query}', 'SearchController@search')->name('search');

// Profile routes

include("profile.php");

// Connections routes
include("connections.php");

// Email Templates routes
include("templates.php");

// Admin routes
include("admin.php");

// testing routes
include("tests.php");