<?php

Route::prefix('admin')->group(function () {
	Route::get('/', 'AdminController@index');
	Route::get('/occupations', 'AdminController@occupations')->name('occupations');
	Route::get('/occupations/remove/{id}', 'AdminController@removeOccupation');
	Route::get('/occupations/rename/{id}/{occupation}', 'AdminController@updateOccupation');

	Route::get('/merge', 'AdminController@merge')->name('mergeOccupations');
	Route::post('/merge', 'AdminController@submitMerge')->name('submitMerge');
});