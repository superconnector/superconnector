<?php

Route::prefix('profile')->group(function () {
	Route::get('/', "ProfileController@index");
	Route::post('/', "ProfileController@update");
});