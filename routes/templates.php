<?php

Route::prefix('templates')->group(function () {
	Route::get('/', 'EmailTemplatesController@index');
	Route::post('/', 'EmailTemplatesController@new');
	Route::get('/view/{id}', 'EmailTemplatesController@getTemplatePreview');
	Route::get('/data/{id}/{matcher}/{matchee}', 'EmailTemplatesController@getTemplateData');
	Route::get('/delete/{id}', 'EmailTemplatesController@deleteTemplate');
	Route::post('/save/{id}', 'EmailTemplatesController@updateTemplate');
});