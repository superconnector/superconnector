<?php

Route::prefix('tests')->group(function () {
	Route::prefix('emails')->group(function () {
		Route::get('/genericSend', 'TestingController@emailGeneric');
	});
});	