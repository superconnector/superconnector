<?php

use App\Occupation as Occupations;


Route::post('/connections', 'ConnectionsController@insert');
Route::post('/importContacts', 'ConnectionsController@importContacts');

Route::prefix('connection')->group(function () {
	Route::get('/view/{id}', 'ConnectionsController@getConnection');
	Route::get('/remove/{id}', 'ConnectionsController@removeConnection');
	Route::get('/favorite/{id}', 'ConnectionsController@favoriteConnect');
	Route::get('/refresh/{matcher}/{matchee}', 'ConnectionsController@newTop30');
	Route::get('/skip/{matcher}/{skip}', 'ConnectionsController@skipConnections');
	Route::get('/verify/potential/{id}', 'ConnectionsController@hasPotentialMatches');

	Route::post('/{id}', 'ConnectionsController@updateConnection');
});

Route::prefix('matches')->group(function () {
	Route::get('/find/{id?}', 'ConnectionsController@getMatches');
	Route::get('/manualOffline/finish/{id}', 'ConnectionsController@manualOfflineMatchFinish')->name('manual-offline-match-finish');
	Route::get('/manualOffline/remind/{id}', 'ConnectionsController@manualOfflineMatchRemind')->name('manual-offline-match-remind');
	
	Route::post('/manualOffline', 'ConnectionsController@manualOfflineMatch')->name('manual-offline-match');
	Route::post('/manual', 'ConnectionsController@manualMatch')->name('manual-match');
	Route::post('/manualPersonal', 'ConnectionsController@manualPersonalMatch')->name('manual-personal-match');
});




Route::get('/match/{matcher}/{matchee}', 'ConnectionsController@introduce');
Route::get('/matchtop30/{matcher}/{matchee}', 'ConnectionsController@introduceTop30');

Route::post('/match/{matcher}/{matchee}', 'ConnectionsController@makeIntroduction');

Route::get('/top', 'ConnectionsController@getTop30');
Route::get('/opportunities', 'ConnectionsController@getOpportunities');

Route::post('/top/import', 'ConnectionsController@importTop30')->name('importTop30');

Route::get('/occupations', function(){
	$occupations = [];
	$occupations["title"] = [];
	$occupations["value"] = [];

	foreach (Occupations::orderBy('title', 'ASC')->get() as $occupation) {
		array_push($occupations["title"], $occupation->title);
		array_push($occupations["value"], $occupation->id);
	}
	return json_encode($occupations);
});