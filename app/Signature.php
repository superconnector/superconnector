<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signature extends Model
{
    protected $fillable = [
        'id', 'author', 'content','date', 'version', 'updated_at', 'created_at',
    ];

    protected $table= 'signatures';
}