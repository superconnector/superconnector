<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name','l_name','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin(){
        if($this->admin == 1){
            return true;
        }
        return false;
    }

    public function signature(){
        return $this->hasOne('\App\Signature');
    }

    public function connections(){
        return $this->hasMany('\App\Connection', 'id', 'author');
    }

    public function templates(){
        return $this->hasMany('\App\EmailTemplate');
    }

    public function favorites(){
        return $this->hasMany('\App\Top30');
    }

    public function offlineMatches(){
        return $this->hasMany('\App\OfflineMatch');
    }
}
