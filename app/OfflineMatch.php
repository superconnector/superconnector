<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfflineMatch extends Model
{
	protected $fillable = [
		'user_id', 'matcher','matchee', 'reminded_at', 'notes'
    ];

    protected $table = 'offline_matches';
}
