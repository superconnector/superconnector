<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User as Users;
use App\Connection as Connections;
use Laravel\Scout\Searchable;

class Occupation extends Model
{
    use Searchable;

    protected $table= 'occupations';

    protected $fillable = [
        'title',
        'custom',
        'user_id'
    ];

    public function users(){
    	return $this->belongsToMany('\App\User', 'title');
    }

    public function connections(){
    	return $this->belongsToMany('\App\Connection', 'title');
    }

    public function titles(){
    	return Users::where('title', $this->id)->count() + Connections::where('title', $this->id)->count();
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }

    /**
     * Get the value used to index the model.
     *
     * @return mixed
     */
    
    public function getScoutKey()
    {
        return $this->title;
    }
}