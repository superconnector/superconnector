<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Connection extends Model
{
	use Searchable;

	protected $fillable = [
        'author', 'f_name','l_name','email', 'title', 'company', 'cell', 'work', 'notes', 'opportunities', 'skip',
    ];

    protected $table = 'connections';

}