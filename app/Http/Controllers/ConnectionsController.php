<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as Users;
use App\Connection as Connections;
use App\Signature as Signatures;
use App\EmailTemplate as EmailTemplates;
use App\Occupation as Occupations;
use App\Match as Matches;
use App\OfflineMatch as OfflineMatches;
use App\Opportunity as Opportunities;
use DB;
use Auth;
use Session;
use Redirect;
use Carbon\Carbon;

class ConnectionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function insert(Request $request){
        $lookingInsert = [];
        $title = $request->title;
            if(!is_numeric ($request->title)){
                $title = newTitle($request->title);
        }
        foreach ((array)$request->looking as $looking) {
            if(!is_numeric ($looking)){
               $looking = newTitle($looking);
            }
            array_push($lookingInsert, $looking);
        }


    	DB::table('connections')->insert([
    		'author' => Auth::user()->id,
    		'f_name' => $request->f_name,
    		'l_name' => $request->l_name,
    		'title' => $title,
    		'company' => $request->company,
    		'email' => $request->email,
    		'cell' => $request->cell,
    		'work' => $request->work,
    		'looking' => implode(",", $lookingInsert)
    	]);

    	Session::flash('success', "Successfully added a new connection!");
        return Redirect::to('/home');
    }

    public function getConnection($id){
        /*
            The logged in user wants to know if they were ever matched with someone in the system
        */
        $connection = Connections::where('author', Auth::user()->id)->where('id', $id)->first();

        $user = Users::where('email', $connection->email)->first();
        /*
            The logged in user wants to know if there are pending matches for them by this connection
        */

        $potentialmatches = [];

        $connected = NULL;

        if(!is_null($user)){
            $connected = Connections::where('author', $user->id)->where('email', Auth::user()->email)->first();
            if(!is_null($connected)){
                // get the keywords this user lists
                $matchKeys = explode(",", $connected->looking);
                $results = $this->connectionMatches($connection, $matchKeys);
                if(!empty($results)){
                    // we found matches. show them to the user
                    $potentialmatches = $results;
                }
            }
        }



        $data = [
            "connection" => $connection,
            "occupations" => Occupations::orderBy('title', 'ASC')->get(),
            "opportunities" => Opportunities::orderBy('title', 'ASC')->get(),
            "matches" => Matches::where('author', Auth::user()->id)->where('personal',0)->where('matcher', $id)->orWhere('matchee', $id)->orderBy('id', 'DESC')->get(),
            "matchers" => Connections::where('author', Auth::user()->id)->get(),
            "matchees" => Connections::where('author', Auth::user()->id)->where('id', '!=', $id)->get(),
            "personal_matches" => Matches::where('author', $id)->where('personal', Auth::user()->id)->orderBy('id', 'DESC')->get(),
            "potential_matches" => $potentialmatches,
            "connected" => $connected,
            "offline_matches" => OfflineMatches::where('user_id', Auth::user()->id)->where('matcher', $id)->get(),
            "page" => "contacts"
        ];

        return view('connections.edit', $data);
    }

    public function getMatches($id = NULL){
        $matches = [];

        $connections = Connections::where('looking','!=','')->where('email','!=', NULL)->where('author', Auth::user()->id)->get();
        
        if(empty($id)){
            $connection = $connections->first();
        } else {
            $connection = $connections->find($id);
        }
        // find bonding pairs
        
        $matchKeys = explode(",", $connection->looking);
        $results = $this->connectionMatches($connection, $matchKeys);
        if(!empty($results)){
            // we found matches. show them to the user
            $matches = array_merge($matches, $results);
        }

        $data = [
            "connections" => $connections,
            "matches" => $matches,
            "occupations" => Occupations::orderBy('title', 'ASC')->get(),
            "page" => "matches",
            "contact" => $id
        ];
        

        return view('connections/matches', $data);
    }

    public function introduce($matcher, $matchee){

        $data = [
            "matcher" => Connections::where('id', $matcher)->where('author', Auth::user()->id)->first(),
            "matchee" => Connections::where('id', $matchee)->where('author', Auth::user()->id)->first(),
            "signatures" => Signatures::where('author', Auth::user()->id)->get(),
            "templates" => EmailTemplates::where('author', Auth::user()->id)->get(),
            "top30" => false,
            "page" => "matches"
        ];

        return view('/connections/introduce', $data);
    }

    public function introduceTop30($matcher, $matchee){

        $data = [
            "matcher" => Connections::where('id', $matcher)->where('author', Auth::user()->id)->first(),
            "matchee" => Connections::where('id', $matchee)->where('author', Auth::user()->id)->first(),
            "signatures" => Signatures::where('author', Auth::user()->id)->get(),
            "templates" => EmailTemplates::where('author', Auth::user()->id)->get(),
            "top30" => true,
            "page" => "matches"
        ];

        return view('/connections/introduce', $data);
    }

    public function manualMatch(Request $request){

        $matchee = $request->matchee;

        if(isset($request->newmatchemail)){
            // create new contact
            $name = explode(" ", $request->matchee);
            $lname = null;
            $fname = $name[0];
            if(array_key_exists(1, $name)){
                $lname = $name[1];
            }

            $newconnection = DB::table('connections')->insert([
                'author' => Auth::user()->id,
                'f_name' => $fname,
                'l_name' => $lname,
                'company' => $request->newmatchcompany,
                'email' => $request->newmatchemail,
                'notes' => $request->notes
            ]);

            $matchee = DB::getPdo()->lastInsertId();
        }

        // prevent nonnumeric connection from entering database

        if(is_numeric($matchee)){
            DB::table('matches')->insert([
                "author" => Auth::user()->id,
                "template" => 0,
                "matcher" => $request->matcher,
                "matchee" => $matchee,
                'notes' => $request->notes,
                "date" => convertHTMLTimetoUnix($request->date)
            ]);

            Session::flash('success', "Manual connection saved");

        } else {
            Session::flash('error', "Missing data for new contact");
        }
        
        return Redirect::back();
    }

    public function manualPersonalMatch(Request $request){
        $matcher = $request->matcher;

        if(isset($request->newmatchemail)){
            // create new contact
            $name = explode(" ", $request->matcher);
            $lname = null;
            $fname = $name[0];
            if(array_key_exists(1, $name)){
                $lname = $name[1];
            }

            $newconnection = DB::table('connections')->insert([
                'author' => Auth::user()->id,
                'f_name' => $fname,
                'l_name' => $lname,
                'company' => $request->newmatchcompany,
                'notes' => $request->notes,
                'email' => $request->newmatchemail
            ]);

            $matcher = DB::getPdo()->lastInsertId();
        }

        if(is_numeric($matcher)){

            DB::table('matches')->insert([
                "author" => $request->author,
                "template" => 0,
                "matcher" => $matcher,
                "matchee" => Auth::user()->id,
                "personal" => Auth::user()->id,
                'notes' => $request->notes,
                "date" => convertHTMLTimetoUnix($request->date)
            ]);

            Session::flash('success', "Manual connection saved");
        }
        else {
            Session::flash('error', "Missing data for new contact");
        }
        
        return Redirect::back();
    }

    public function manualOfflineMatch(Request $request){
        // create the person as a contact

        // create the person in the offline/non-user system
        $author = $request->author;
        $request->merge(['author' => Auth::user()->id]);
        $connection = Auth::user()->connections()->create($request->all());

        $request->merge(['matchee' => $connection->id]);


        Auth::user()->offlineMatches()->create($request->all());

        Session::flash('success', "Manual connection saved and we added " . $request->f_name . " " . $request->l_name . " to your contacts");

        return Redirect::back();
    }

    public function manualOfflineMatchFinish($id){
        DB::table('offline_matches')->where('user_id', Auth::user()->id)->where('id', $id)->update([
            "status" => 1
        ]);
    }
    public function manualOfflineMatchRemind($id){

        $match = OfflineMatches::find($id);

        $matcher = Connections::find($match->matcher);
        $matchee = Connections::find($match->matchee);

        

        sendReminderEmail($matcher, $matchee);

        DB::table('offline_matches')->where('user_id', Auth::user()->id)->where('id', $id)->update([
            "reminded_at" => Date("Y-m-d H:m:s")
        ]);
    }

    public function makeIntroduction(Request $request, $matcher, $matchee){

        $matcher = Connections::where('id', $matcher)->where('author', Auth::user()->id)->first();
        $matchee = Connections::where('id', $matchee)->where('author', Auth::user()->id)->first();

        $signature = Signatures::where('author', Auth::user()->id)->where('version', $request->version)->first();

        if(!$signature){
            $signature = '<!DOCTYPE html>
                <html>
                <head>
                </head>
                <body>
                <p>Yours truly, <br />' . $request->from_fname . ' ' . $request->from_lname .'</p>
                </body>
                </html>';
        } else {
            $signature = $signature->content;
        }

        $content = str_replace ('<br>' , '\r\n', "$request->content \r\n\r\n $signature");

        $data = [
            "matcher" => $matcher,
            "matchee" => $matchee,
            "content" => $content,
            "test" => false,
            "page" => "matches"
        ];

        if($request->sendtest == "Send Test to Myself"){
            // I am sending a test email to myself
            $data["test"] = true;
            sendIntroductionEmail(Auth::user()->email,"$matcher->f_name $matcher->l_name", $request->from, $content, $request->subject, $request->from, $request->from_fname, $request->from_lname);
        } else {
            // send the real thing
            sendIntroductionEmail($matcher->email,"$matcher->f_name $matcher->l_name", $matchee->email, $content, $request->subject, $request->from, $request->from_fname, $request->from_lname);
            if($request->top30 == "top30"){
                DB::table('top30')->insert([
                    'author' => Auth::user()->id,
                    'connection' => $matcher->id
                ]);
            }
        }

        // save record of the match
        DB::table('matches')->insert([
            "author" => Auth::user()->id,
            "template" => $request->template,
            "matcher" => $matcher->id,
            "matchee" => $matchee->id
        ]);

        return view('/connections/success', $data);
    }

    public function updateConnection(Request $request, $id){
        
        $lookingInsert = [];
        $title = $request->title;
            if(!is_numeric($request->title)){
                $title = newTitle($request->title);
        }
        foreach ((array) $request->looking as $looking) {
            if(!is_numeric ($looking)){
               $looking = newTitle($looking);
            }
            array_push($lookingInsert, $looking);
        }

        $opportunitiesInsert = [];

        foreach ((array) $request->opportunities as $opportunity) {
            if(!is_numeric ($opportunity)){
               $opportunity = newOpportunity($opportunity);
            }
            array_push($opportunitiesInsert, $opportunity);
        }
        
        DB::table('connections')->where('id', $id)->where('author', Auth::user()->id)->update([
            'author' => Auth::user()->id,
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'title' => $title,
            'company' => $request->company,
            'email' => $request->email,
            'cell' => $request->cell,
            'work' => $request->work,
            //'industry' => $request->industry,
            'looking' => implode(",", $lookingInsert),
            'opportunities' => implode(",", $opportunitiesInsert)
        ]);

        

        Session::flash('success', "Successfully updated this connection!");
        return Redirect::to('/connection/view/' . $id);
    }

    public function removeConnection($id){
        DB::table('connections')->where('id', $id)->where('author', Auth::user()->id)->delete();
    }

    public function favoriteConnect($id){
        
        $connection = Connections::where('id', $id)->first();
        if($connection->favorite == 1){
            return DB::table('connections')->where('id', $id)->where('author', Auth::user()->id)->update(["favorite" => 0]);
        }
        else{
            $favoritesCount = Connections::where('author', Auth::user()->id)->where('favorite',1)->count();

            /* 
            if($favoritesCount >= 30){
                return 30;
            }
            */

           return DB::table('connections')->where('id', $id)->where('author', Auth::user()->id)->update(["favorite" => 1]); 
        }
        
    }

    public function getTop30(){
        $topMatches = [];

        //get all my favorites
        $connections = Connections::where('author', Auth::user()->id)->where('favorite', 1)->get();

        foreach($connections as $connection){
            $available = 0;
            $matchKeys = explode(",", $connection->looking);

            if(verifyMonthlyComplete($connection->id) == 0){
                // the user has not made any matches this month

                $matches = $this->connectionMatches($connection, $matchKeys);

                if(isset($matches)){
                    // the user has potential matches
                    $available = sizeof($matches);
                    if($available > 0){
                        for($i = 0; $i < $available; $i++){
                            $matcher = Connections::where('id', $matches[$i][0])->first();
                            $matchee = Connections::where('id', $matches[$i][1])->first();

                            // check if match not already made

                            if(!hasMatchMailSent($matcher->id, $matchee->id) && $matcher->id != $matchee->id){
                                array_push($topMatches, $matches[$i]);
                                break;
                            }
                        }
                    }
                }
            }

        }

        $data = [
            "connections" => $topMatches,
            "occupations" => Occupations::orderBy('title', 'ASC')->get(),
            "page" => "top30"
        ];



       return view('connections.top30', $data);

    }

    public function newTop30($matcher, $matchee){
        $oem = $matchee;
        $matches =  array();
        $newMatch = [];
        // get all of my connections where I know who they are looking for
        $connection = Connections::find($matcher);

        // find bonding pairs
        
        $matchKeys = explode(",", $connection->looking);

        // run a do while loop to always return someone unique

        $z = 0;  // number of attempts before an error is thrown
        do {
            
            $results = $this->connectionMatches($connection, $matchKeys);
            if(!empty($results)){
                // we found matches. show them to the user


                $matches = $results;
            }

            for($i=0; $i < sizeof($matches); $i++){

                if(is_array($matches[$i])){

                    if((string) $matches[$i][1] == (string) $matchee){
                        unset($matches[$i]);
                    }
                }
            }

            shuffle($matches);
            
            if($matchee != Connections::find($matches[0])){
                $z == 11;
            }

            $matches = $matches[0];

            $connection = Connections::find($matches[0]); 
            $matchee = Connections::find($matches[1]); 

            
            $z++;
        }
        while ($z < 10);

        if(isset($matchee) && $oem != $matchee->id){
            $html = "<i class='fa fa-check'></i> Introduced";
            
            if(!hasMatchMailSent($connection->id, $matchee->id)){
               $html = '<a href="/matchtop30/' . $connection->id . '/' . $matchee->id .
                '" target="_blank">Finish Introduction <i class="fa fa-envelope"></i></a></a>';

                $matches = [
                    "connectionName" => $connection->f_name . " " . $connection->l_name,
                    "connectionTitle" => getOccupationTitle($connection->title),
                    "lookingFor" => getOccupationTitle($matches[4]),
                    "connectionOpp" => '<a href="/connection/view/' . $matchee->id . '" target="_blank">' . $matchee->f_name . " " . $matchee->l_name . 
                    '</a><br>
                    <b> ' . getOccupationTitle($matchee->title) . 
                    '</b> <br>
                     ' . $html,
                    "relevance" => $matches[3],
                    "matcher" => $connection->id,
                    "matchee" => $matchee->id
                ];
            } else {
                $matches = [];
            }
        }
        else {
            $matches = ["error" => true];
        }

        return json_encode($matches);
    }

    public function importContacts(Request $request){
        $this->uploadTemplate($request, 0);
        return Redirect::to('/home');
    }

    public function importTop30(Request $request){
        $this->uploadTemplate($request, 1);
        return Redirect::to('/top');
    }

    public function uploadTemplate($request, $favorite){
        $destination = public_path() . "/uploads/";
         if($request->file('top30') !== null){
            $top30 = $request->file('top30');
            $top30Name = generateRandomString() . "-" . $top30->getClientOriginalName();
            $top30->move($destination, $top30Name); 

            $rows = [];
            $row = 1;
            $imported = 0;
            if (($handle = fopen(public_path() . "/uploads/$top30Name", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                    $num = count($data);
                    
                    $rows[$row] = array();
                    for ($c=0; $c < $num; $c++) {
                        array_push($rows[$row], $data[$c]);
                        if($data[0] != "First Name"){
                            $lookingInsert = [];
                            $title = $data[2];
                                if(!is_numeric ($data[2])){
                                    $title = newTitle($data[2]);
                            }
                            foreach (explode(",", $data[7]) as $looking) {
                                if(!is_numeric ($looking)){
                                   $looking = newTitle($looking);
                                }
                                array_push($lookingInsert, $looking);
                            }

                            if(!empty($data[4]) && DB::table('connections')->where('author', Auth::user()->id)->where('email', $data[4])->count() == 0){

                                DB::table('connections')->insert([
                                    'author' => Auth::user()->id,
                                    'f_name' => $data[0],
                                    'l_name' => $data[1],
                                    'title' => $title,
                                    'company' => $data[3],
                                    'email' => $data[4],
                                    'cell' => $data[5],
                                    'work' => $data[6],
                                    'industry' => NULL,
                                    'looking' => implode(",", $lookingInsert),
                                    'favorite' => $favorite
                                ]);

                                $imported++;

                            } else {
/*
                                DB::table('connections')->where('author', Auth::user()->id)->where('email', $data[4])->update([
                                    'title' => $title
                                ]);
                                */
                            }

                        }
                        break;
                    }
                   $row++;
                }
                fclose($handle);
            }

            $data = [
                "rows" => $rows,
                "page" => "top30"
            ];

            /* 
            if(Connections::where('author', Auth::user()->id)->where('favorite',1)->count() >= 30){
                $diff = sizeof($rows) - $imported - 1;
                Session::flash('success', "<div class='alert alert-info'>You tried to upload $diff connections, but $imported were allowed due to reaching the maximum limit of 30 total favorite connections.</div>");
            } else {
                Session::flash('success', "<div class='alert alert-success'>Successfully imported $imported connections</div>");
            }
            */

            Session::flash('success', "<div class='alert alert-success'>Successfully imported $imported connections</div>");

         } else 
         {
            echo "no file";
         }
    }


    public function search($query){
        $words = explode(" ", $query);
        $url = env("ELASTICSEARCH_HOST") . "/title/_search?q=" . str_replace(" ", "+", stringClean($query));
        $occupations = json_decode(file_get_contents($url), true)["hits"];
        
        $matches = [];

        foreach ($occupations as $results) {
            if(is_array($results)){
                foreach ($results as $result) {
                    if(array_key_exists("title", $result["_source"])){
                        $score = $result["_score"];
                        $chunks = explode(" ", $result["_source"]["title"]);
                        if(strtoupper($words[0]) != strtoupper($chunks[0])){
                            $score = $score * 0.9;
                        }

                        if (!empty($words[0]) && strpos($result["_source"]["title"], $words[0]) !== false) {
                            $score = $score * 1.4;
                        }
                        array_push($matches, ["match" => $result["_source"]["title"], "score" => $score]);
                    }
                }
            }
        }

        usort($matches, function($a, $b) {
            return $b['score'] <=> $a['score'];
        });

        return $matches;
    }

    public function connectionMatches($connection, $matchKeys){
        DB::enableQueryLog();
        $matches = [];
        $i = 0;
        foreach ($matchKeys as $matchKey) {
            $matchesFound = DB::table('connections')->whereRaw("FIND_IN_SET('".$matchKey."',title)")->where('author', Auth::user()->id)->where('email', '!=', Auth::user()->email)->where('looking','!=','')->get();
            
            foreach ($matchesFound as $matchFound) {
                $matches[$i] = [];
                array_push($matches[$i], "$connection->id");
                array_push($matches[$i], "$matchFound->id");
                array_push($matches[$i], "$matchKey");
                array_push($matches[$i], "Exact");
                array_push($matches[$i], $matchKey);
                array_push($matches[$i], 15);
                $i++;
            }

            // widen the net to include semblance results
            if(Occupations::find($matchKey) !== null){       

                $searchResults = $this->search(Occupations::find($matchKey)->title);

                foreach($searchResults as $searchResult){
                    $occupationID = Occupations::where('title', $searchResult["match"])->first();
                    if(!is_object($occupationID)){
                        //dd($searchResult["match"]);
                    } else {
                        $occupationID = $occupationID->id;
                        if($occupationID != $matchKey && $searchResult["score"] > env('ELASTIC_WEAK')){

                        $matchesFound = DB::table('connections')->whereRaw("FIND_IN_SET('".$occupationID."',title)")->where('author', Auth::user()->id)->where('email', '!=', Auth::user()->email)->where('looking','!=','')->get();

                        

                        foreach ($matchesFound as $matchFound) {
                            $score = $this->modifyScore($searchResult["score"], $matchKey, $matchFound->id);
                            $score = weighResultScore($score);

                            if(is_object($connection) && $connection->id != $matchFound->id){
                                $matches[$i] = [];
                                array_push($matches[$i], "$connection->id");
                                array_push($matches[$i], $matchFound->id);
                                array_push($matches[$i], $occupationID);
                                array_push($matches[$i], $score);
                                array_push($matches[$i], $matchKey);
                                array_push($matches[$i], $searchResult["score"]);
                                $i++;
                            }
                        }
                    }
                    }
                }
            }
        }
        return $matches;
    }

    function getMonthlyConnection($connectionID, $looking){
        $matches = [];
        $matchKeys = explode(",", $looking);

        $x = 0;

        foreach ($matchKeys as $matchKey) {
            
            $firstofmonth = Carbon::now()->startOfMonth()->subMonth("+" . $x);

            $matchesFound = DB::table('connections')->whereRaw("FIND_IN_SET('".$matchKey."',title)")->where('author', Auth::user()->id)->where('email', '!=', Auth::user()->email)->where('looking','!=','')->where('date', '>=', $firstofmonth)->get();
            $i = 0;
            foreach ($matchesFound as $matchFound) {
                // make sure this introduction is unique
                $push = true;
                if(Matches::where('personal',0)->where('matcher',$connectionID)->Where('matchee', $matchFound->id)->count() > 0){
                    $push = false;
                }
                if(Matches::where('personal',0)->where('matchee',$connectionID)->Where('matcher', $matchFound->id)->count() > 0){
                    $push = false;
                }
                if($push){
                    $matches[$i] = [];
                    array_push($matches[$i], "$connectionID");
                    array_push($matches[$i], "$matchFound->id");
                    array_push($matches[$i], "$matchKey");
                    break;
                }
                $i++;
            }
            if(sizeof($matches) > 0){
                $x = 0;
                break;
            }
            
        }

        return $matches;
    }

    function skipConnections($matcher, $skip){
        $connection = Connections::where('id', $matcher)->where('author', Auth::user()->id)->first();
        $connection->skip = $skip;
        $connection->save();
    }

    function modifyScore($score, $lookingfor, $matchee){
        $matchee = Connections::find($matchee);
        $matcheeLookingFor = explode(',',$matchee->looking);
        if(!in_array($lookingfor, $matcheeLookingFor)){
            $score = $score * .75;
        } else {
            $score = $score * 1.25;
        }

        return $score;
    }

    public function hasPotentialMatches($id){
        $total = 0;

        
        
        $connection = Connections::where('author', Auth::user()->id)->where('id', $id)->first();

        $user = Users::where('email', $connection->email)->first();
        
            //The logged in user wants to know if there are pending matches for them by this connection
        

        $potentialmatches = [];

        $connected = NULL;

        if(!is_null($user)){
            $connected = Connections::where('author', $user->id)->where('email', Auth::user()->email)->first();
            if(!is_null($connected)){
                // get the keywords this user lists
                $matchKeys = explode(",", $connected->looking);
                $results = $this->connectionMatches($connection, $matchKeys);
                $potentialmatches = $results;
                $total = $results->count();

            }
        }

        $offlineMatches = OfflineMatches::where('user_id', Auth::user()->id)->where('matcher', $id)->where('status', 0)->get();

        foreach($offlineMatches as $offlineMatch){
            if(Connections::where('id', $offlineMatch->matchee)->count() > 0){
                $total++;
            }
        }

        return $total;
    }

    public function getOpportunities(){
        $connections = Connections::where('author', Auth::user()->id)->get();
        $opportunities = [];

        foreach($connections as $connection){
            $oppCount = $this->hasPotentialMatches($connection->id);

            if($oppCount > 0){
                array_push($opportunities, $connection);
            }
        }

        $data = [
            "connections" => $opportunities,
            "page" => "opportunities"
        ];

        return view("connections.opportunities", $data);
    }
}
