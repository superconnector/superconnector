<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Connection as Connections;
use App\Occupation as Occupations;
use App\User as Users;
use App\Match as Matches;
use Auth;
use DB;
use Session;
use Redirect;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = [
            "users" => Users::all(),
            "connections" => Connections::all(),
            "matches" => Matches::all(),
            "page" => "admin"
        ];

        return view('admin.index', $data);
    }

    public function occupations(){
        $data = [
            "occupations" => Occupations::all(),
            "page" => "admin"
        ];

        return view('admin.occupations', $data);
    }

    public function merge(){
        $data = [
            "occupations" => Occupations::all(),
            "page" => "admin"
        ];

        return view('admin.merge', $data);
    }


    public function removeOccupation($id){
        DB::table('occupations')->where('id', $id)->delete();

    }

    public function updateOccupation($id, $title){
        DB::table('occupations')->where('id', $id)->update([
            'title' => $title
        ]);
    }

    public function submitMerge(Request $request){
        // mass reassignment of user titles

        $connections = Connections::where('title', '!=', NULL)->get();

        foreach($connections as $connection){
            if($connection->title == $request->old){
                // updates all of the titles for the user's connections
                DB::table('connections')->where('id', $connection->id)->update([
                    'title' => $request->new
                ]);
            }
            if($connection->looking != NULL){
                // updates all of looking for titles to match the merged ID

                $lookingFors = explode(",", $connection->looking);
                $lookingFor = arrayReplace($request->old, $request->new, $lookingFors);
                $looking = implode(",", $lookingFor);

                DB::table('connections')->where('id', $connection->id)->update([
                    'looking' => $looking
                ]);
            }

            DB::table('occupations')->where('id', $request->old)->delete();
        }

        Session::flash('success', "Successfully merged occupations!");

        return Redirect::to('/admin/merge');
    }

}
