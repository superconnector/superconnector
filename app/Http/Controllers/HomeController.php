<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Connection as Connections;
use App\Occupation as Occupations;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $occupations = "";

        foreach(Occupations::all() as $occupation){
            $occupations .= "<option value='$occupation->id'>$occupation->title</option>";
        }

        $data = [
            "connections" => Connections::where('author', Auth::user()->id)->get(),
            "occupations" => $occupations,
            "page" => "contacts"
        ];
        return view('home', $data);
    }

}
