<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Signature as Signatures;
use App\Occupation as Occupations;
use Auth;
use Session;
use DB;
use Redirect;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            "signatures" => Signatures::where('author', Auth::user()->id)->get(),
            "occupations" => Occupations::orderBy('title', 'ASC')->get(),
            "page" => "profile"
        ];
        return view('/profile/index', $data);
    }

    public function update(Request $request)
    {
        if(User::where('id','!=', Auth::user()->id)->where('email', $request->email)->count() == 0){
            $title = $request->title;
            if(!is_numeric($request->title)){
                $title = newTitle($request->title);
            }
            DB::table('users')->where('id', Auth::user()->id)->update([
                'f_name' => $request->f_name,
                'l_name' => $request->l_name,
                'title' => $title,
                'company' => $request->company,
                'phone' => $request->phone,
                'email' => $request->email
            ]);

            // multiple signature handler

            $signatures = $request->signature;

            for($i = 0; $i < sizeof($signatures); $i++){

                $attributes = [
                    'author' => Auth::user()->id,
                    'version' => $i
                ];

                $values = [
                    'author' => Auth::user()->id,
                    'content' => $signatures[$i],
                    'version' => $i
                ];

                Signatures::updateOrCreate($attributes, $values);
                
            }

           
            

            if(!empty($request->password)){
                $newpass = bcrypt($request->password);
                DB::table('users')->where('id', Auth::user()->id)->update([
                    'password' => $newpass
                ]);
            }

            Session::flash('success', "Your profile has been updated");
            return Redirect::to('/profile');
        }
        Session::flash('error', "This email is in use by someone else");
        return Redirect::back();
    }
}
