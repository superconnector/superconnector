<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Occupation as Occupations;

class SearchController extends Controller
{
    public function search($query){
    	$words = explode(" ", $query);
    	$url = env("ELASTICSEARCH_HOST") . "/title/_search?q=" . str_replace(" ", "+", $query);
   		$occupations = json_decode(file_get_contents($url), true)["hits"];
   		echo "<h3>Searching for $query</h3> <pre>";
   		
   		$matches = [];

   		foreach ($occupations as $results) {
   			if(is_array($results)){
	   			foreach ($results as $result) {
	   				$score = $result["_score"];
	   				$chunks = explode(" ", $result["_source"]["title"]);
	   				if(strtoupper($words[0]) != strtoupper($chunks[0])){
	   					$score = $score * 0.9;
	   				}
	   				if (strpos($result["_source"]["title"], $words[0]) !== false) {
					    $score = $score * 1.4;
					}
	   				array_push($matches, ["match" => $result["_source"]["title"], "score" => $score]);
	   			}
   			}
   		}

   		usort($matches, function($a, $b) {
		    return $b['score'] <=> $a['score'];
		});

		dd($matches);
	}
}
