<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Connection as Connections;
use App\Signature as Signatures;
use App\EmailTemplate as EmailTemplates;
use DB;
use Auth;
use Session;
use Redirect;

class EmailTemplatesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index(){
		$data = [
			"templates" => EmailTemplates::where('author', Auth::user()->id)->get(),
			"page" => "templates"
		];

		return view('templates/index', $data);
	}

	public function new(Request $request){
		DB::table('email_templates')->insert([
			"author" => Auth::user()->id,
			"name" => $request->name,
			"subject" => $request->subject,
			"content" => $request->content
		]);

		Session::flash('success', "You successfully created a new template");
        return Redirect::to('/templates');
	}

	public function getTemplatePreview($id){
		$data = [
			"template" => EmailTemplates::where('author', Auth::user()->id)->where('id', $id)->first(),
			"page" => "templates"
		];

		return view('templates/template', $data);
	}

	public function getTemplateData($id, $matcher, $matchee){
		$template = ["subject" => NULL, "content" => NULL];
		$templateData = EmailTemplates::where('author', Auth::user()->id)->where('id', $id)->first();
		$matcher = Connections::where('id', $matcher)->where('author', Auth::user()->id)->first();
        $matchee = Connections::where('id', $matchee)->where('author', Auth::user()->id)->first();
        $template["content"] = templateVarSwap($templateData->content, $matcher, $matchee);
        $template["subject"] = str_replace("&nbsp;"," ",strip_tags(templateVarSwap($templateData->subject, $matcher, $matchee)));
		return $template;
	}

	public function updateTemplate(Request $request, $id){
		DB::table('email_templates')->where('author', Auth::user()->id)->where('id', $id)->update([
			"name" => $request->name,
			"subject" => $request->subject,
			"content" => $request->content
		]);

		Session::flash('success', "Success! This template has been updated.");
        return Redirect::to('/templates/view/' . $id);
	}

	public function deleteTemplate($id){
		DB::table('email_templates')->where('author', Auth::user()->id)->where('id', $id)->delete();
	}
}