<?php

use Illuminate\Support\Facades\Mail;

function sendIntroductionEmail($emailto, $emailtoName, $emailCC, $content, $subject, $from = null, $fname, $lname){

    if($from == null){
        $from = Auth::user()->email;
    }

    $myname = $fname . " " . $lname;
    $content = str_replace ('<br>' , '

', $content);
    $content = str_replace ("&#10;" , '

', $content);
    $data = array(
        "name" => $myname,
        "title" => "Superconnector Match",
        "content" => $content
    );
    $emailCC = [$emailCC, $from];

    Mail::send(['html' => 'emails.simpleIntroduction'], $data, function($message) use ($emailto, $emailtoName, $emailCC, $from, $subject, $myname){
        $message->to($emailto, $emailtoName)->cc($emailCC)->subject($subject);
        $message->from($from, $myname);
        $message->sender($from, $myname);
    });
}

function sendReminderEmail($matcher, $matchee){
    $emailto = $matcher->email;
    $emailtoName = getMatchName($matcher->id);
    $myname = Auth::user()->f_name . " " . Auth::user()->l_name;
    $from = Auth::user()->email;
    $subject = "Following up on introducing me to $matchee->f_name";
    $data = [
        "matcher" => $matcher,
        "matchee" => $matchee,
        "title" => "Following up on introducing me to $matchee->f_name",
        "myname" => $myname
    ];

    Mail::send(['html' => 'emails.simpleReminder'], $data, function($message) use ($emailto, $emailtoName, $from, $subject, $myname){
        $message->to($emailto, $emailtoName)->bcc($from)->subject($subject);
        $message->from($from, $myname);
        $message->sender($from, $myname);
    });
}

// email helpers