<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as HTTP_Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\BadResponseException;
use Carbon\Carbon;
use App\User as User;
use App\Connection as Connections;
use App\Occupation as Occupations;
use App\Match as Matches;
use App\Top30 as Top30;

function formatGMTstring($date, $format){
    $date =  new DateTime($date);
    return $date->format($format);
}

function dateToISO($date){
    $date =  new DateTime($date);
    return $date->format("c");
}

function DateDifference($start, $end){
    $datetime1 = new DateTime($start);
    $datetime2 = new DateTime($end);
    $interval = $datetime1->diff($datetime2);
    return $interval->format('%a');
}

function getFullName($id){
    $user = User::where('id', $id)->first();
    return $user->fname . " " . $user->lname;
}

function getMatchName($id){
    $contact = Connections::find($id);
    if(!is_object($contact)){
        return "Removed";
    }
    return $contact->f_name . " " . $contact->l_name;
}

function getMatchTitle($id){
    $contact = Connections::find($id);
    if(!is_object($contact)){
        return "Removed";
    }
    return getOccupationTitle($contact->title);
}

function getMatchCompany($id){
    $contact = Connections::find($id);
    if(!is_object($contact)){
        return "Removed";
    }
    return $contact->company;
}

function convertHTMLTime($stamp)
{
    
    if(!is_null($stamp)){
        return date("m-d-Y", strtotime($stamp));
    }
    return "";
}

function convertTimestamp($stamp)
{
    if(!is_null($stamp)){
        return date('M j Y g:i A', strtotime($stamp));
    }
    return "";
}

function convertHTMLTimetoUnix($date){
// takes $formated_datetime and converts to "UNIX timestamp":
$unix_timestamp = STRTOTIME($date);
 
// converts $unix_timestamp to "normal" formated_datetime:         
return DATE("Y-m-d H:i:s",$unix_timestamp); 
}

function convertHTMLTime2($date){
    $parts = explode('-',$date);
    $yyyy_mm_dd = $parts[2] . '-' . $parts[0] . '-' . $parts[1];
    return $yyyy_mm_dd;
}

function exposeArray($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function bsAlert($alert, $class){
    $alertID = rand(1111,9999);
    echo '<div class="alert alert-' . $class . ' alert-styled-left" id="alert1">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            ' . $alert . '
        </div>
        <script>setTimeout("alertHide(1)", 3000); </script>
        ';
}

function displayMatch($matcher, $matchee){
    $matcher = Connections::where('id', $matcher)->first()->toArray();
    $matchee = Connections::where('id', $matchee)->first()->toArray();
    return ["matcher" => $matcher, "matchee" => $matchee];
}

function hasMatchMailSent($matcher, $matchee){
    if(Matches::where('author', Auth::user()->id)->where('matchee', $matchee)->where('matcher', $matcher)->count() > 0 || Matches::where('author', Auth::user()->id)->where('matcher', $matchee)->where('matchee', $matcher)->count() > 0){
        return true;
    }
    return false;
}

function getOccupation($id){
    $occupation = Occupations::where('id', $id)->first();
    if(is_object($occupation)){
        return $occupation->title;
    }
    return "";
}
function getOccupationGrammer($id){
    $occupation = Occupations::where('id', $id)->first();
    $vowels = ["a","e","i","o","u","A","E","I","O","U"];
    if(in_array($occupation->title[0], $vowels)){
        return "an $occupation->title";
    }
    return "a " . $occupation->title;
}

function daysSinceLastIntro($id){
    $matches = Matches::where('matcher', $id)->orWhere('matchee', $id)->orderBy('date', 'desc')->first();
   // return convertTimeStampToNormal($matches->date) . " " . Date("m-d-Y");
    return DateDifference(convertTimeStampToNormal($matches->date), Date("Y-m-d"));
}

function templateVarSwap($text, $matcher, $matchee){
    $matcherVars = array('[FIRST_NAME]', '[LAST_NAME]', '[TITLE]', '[COMPANY]', '[CELL_PHONE]', '[WORK_PHONE]', 
        '<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: bold; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">',
        '</span>');
    $matcheeVars = array('[FIRST_NAME2]', '[LAST_NAME2]', '[TITLE2]', '[COMPANY2]', '[CELL_PHONE2]', '[WORK_PHONE2]', 
        '<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: bold; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">', 
        '</span>');
    $trimOuts = array('[',']','1', '2', '_phone', 'ast', 'irst', 
        '<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: bold; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">', 
        '<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: bold; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">',
        '</span>','&nbsp;');
    // replace all instances of each 
    foreach($matcherVars as $matcherVar){
        $dbVar = strtolower($matcherVar);
        
        foreach ($trimOuts as $trimOut) {
            $dbVar = str_replace($trimOut, "", $dbVar);
            $dbVar = str_replace("&nbsp;", " ", $dbVar);

        }
        if($dbVar == "title"){
            $text = str_replace($matcherVar, getOccupationTitle($matcher->$dbVar), $text);
            
        } else {
            $text = str_replace($matcherVar, $matcher->$dbVar, $text);
        }      

    }
    
    foreach($matcheeVars as $matcheeVar){
        $dbVar = strtolower($matcheeVar);

        foreach ($trimOuts as $trimOut) {
            $dbVar = str_replace($trimOut, "", $dbVar);
            $dbVar = str_replace('&nbsp;', " ", $dbVar);
        }
        if($dbVar == "title"){
            $text = str_replace($matcheeVar, getOccupationTitle($matchee->$dbVar), $text);
            
        } else {
            $text = str_replace($matcheeVar, $matchee->$dbVar, $text);
        }     
    }

    //dd($template . " " . $content);
    
    return $text;

    
}

function countTasks(){
        $matches = [];

        //get all my favorites
        $connections = Connections::where('favorite', 1)->where('author', Auth::user()->id)->get();

        // find bonding pairs
        $i = 0;
        foreach ($connections as $connection) {
            // get the keywords this user lists
            $matchKeys = explode(",", $connection->looking);

            foreach ($matchKeys as $matchKey) {

                $matchesFound = DB::table('connections')->whereRaw("FIND_IN_SET('".$matchKey."',title)")->where('author', Auth::user()->id)->where('email', '!=', Auth::user()->email)->where('looking','!=','')->get();
                
                foreach ($matchesFound as $matchFound) {
                    // make sure this introduction is unique
                    $push = true;
                    if(Matches::where('matcher',$connection->id)->Where('matchee', $matchFound->id)->count() > 0){
                        $push = false;
                    }
                    if(Matches::where('matchee',$connection->id)->Where('matcher', $matchFound->id)->count() > 0){
                        $push = false;
                    }
                    if($push){
                        $matches[$i] = [];
                        array_push($matches[$i], "$connection->id");
                        array_push($matches[$i], "$matchFound->id");
                        array_push($matches[$i], "$matchKey");
                    }
                    $i++;
                }
            }
            
        }

        return sizeof($matches);
        
    }
function newTitle($title){
    if($title != ""){
        $titleCount = DB::table('occupations')->where('title', $title)->count();
        if($titleCount == 0){
            DB::table('occupations')->insert([
                'title' => $title,
                'custom' => 1,
                'user_id' => Auth::user()->id
            ]);
            return DB::getPdo()->lastInsertId();
        } else {
            return DB::table('occupations')->where('title', $title)->first()->id;
        }    
    }
}

function newOpportunity($title){
    if($title != ""){
        $titleCount = DB::table('opportunities')->where('title', $title)->count();
        if($titleCount == 0){
            DB::table('opportunities')->insert([
                'title' => $title
            ]);
            return DB::getPdo()->lastInsertId();
        } else {
            return DB::table('opportunities')->where('title', $title)->first()->id;
        }    
    }
}



function getOccupationTitle($id){
    $occupation = Occupations::where('id', $id)->first();

    if(is_object($occupation)){
        return $occupation->title;
    } else{

    }

    return "";
}

function verifyMonthlyComplete($connectionID){
    // we check to see if we've made an intro for this person this month already
    $firstofmonth = Carbon::now()->startOfMonth();
    return Top30::where('author', Auth::user()->id)->where('connection', $connectionID)->where('date', '>=', $firstofmonth)->count();
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateRandomNumString($length = 10) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function weighResultScore($score){
    // convert elasticsearch's scoring system to something more human friendly
    switch ($score) {
        case $score > env('ELASTIC_STRONG'):
            return "Strong";
            break;
        case $score > env('ELASTIC_WEAK') && $score <= env('ELASTIC_OKAY'):
            return "Okay";
            break;
        default:
            return "Weak";
            break;
    }
}

function stringClean($url){
    $url = str_replace(";", ",", $url);
    $url = str_replace("w/", "with", $url);
    $url = str_replace("/", " or ", $url);
    $url = str_replace("&", "%26", $url);

    return $url;
}

function arrayReplace ($find, $replace, $array) {
    if (!is_array($array)) {
        return str_replace($find, $replace, $array);
    }

    $newArray = [];
    foreach ($array as $key => $value) {
        $newArray[$key] = arrayReplace($find, $replace, $value);
    }
    return array_unique($newArray);
}