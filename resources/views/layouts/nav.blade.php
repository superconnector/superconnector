        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    @guest
                    <a class="navbar-brand" href="{{ url('/') }}">
                    @else
                    <a class="navbar-brand" href="{{ url('/home') }}">
                    @endguest
                        Super Connector
                    </a>

                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest

                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            @if(Auth::user()->isAdmin())
                                <li class="@if($page == 'admin') active @endif"><a href="{{ URL('/admin') }}">Admin</a></li>
                            @endif
                            <li class="@if($page == 'contacts') active @endif"><a href="{{ URL('/home') }}">Contacts</a></li>
                            <li class="@if($page == 'matches') active @endif"><a href="{{ URL('/matches/find/') }}">Matches</a></li>
                            <li class="@if($page == 'top30') active @endif"><a href="{{ URL('/top') }}">Top Connections</a></li>
                            <li class="@if($page == 'opportunities') active @endif"><a href="{{ URL('/opportunities') }}">Opportunities</a></li>
                            <li class="@if($page == 'templates') active @endif"><a href="{{ URL('/templates') }}">Templates</a></li>
                            <li class="@if($page == 'profile') active @endif"><a href="{{ URL('/profile') }}">Profile</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>