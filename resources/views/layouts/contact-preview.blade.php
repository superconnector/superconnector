@if(is_object(\App\Connection::find($matched["matchee"]["id"])))
<div id="preview{{ $i }}" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">{{ $matched["matchee"]["f_name"] . " " . $matched["matchee"]["l_name"] }} <a href="/connection/view/{{ $matched['matchee']['id'] }}" class="btn btn-primary" target="_blank">View Profile</a></h3>
      </div>
      <div class="modal-body">
        <h4>Looking For:</h4> 
        @php
          $connection = \App\Connection::find($matched["matchee"]["id"]);
        @endphp
        @foreach(explode(",", $connection->looking) as $key => $lookingfor)
            @if($lookingfor != 8920)
            @if(getOccupation($lookingfor) == getOccupation($match[4]))<strong style="text-decoration: underline;">{{ getOccupation($match[4]) }}</strong>@else<span>{{ getOccupation($lookingfor) }}</span>@endif @if($key < sizeof(explode(",", $connection->looking)) -1 && $lookingfor != 8920),@endif
            @endif
        @endforeach
        <div class="panel-heading">
            <h5>Matches I've Made for <strong>{{ $matched["matchee"]["f_name"] }} {{ $matched["matchee"]["l_name"] }}</strong></h5>
        </div>
        <div class="panel-body">
            <div class="d-table">
                <ul class="d-column">
                  <li>Matched with</li>
                  <li>Title</li>
                  <li>Company</li>
                  <li>Date</li>
                </ul>
                @php
                    $thePersonMatches = \App\Match::where('author', Auth::user()->id)->where('personal',0)->where('matcher', $matched["matchee"]["id"])->orWhere('matchee', $matched["matchee"]["id"])->orderBy('id', 'DESC')->get();
                @endphp
                @foreach($thePersonMatches as $thePersonMatch)
                    <ul class="d-row">
                        @if($matched["matchee"]["id"] == $thePersonMatch->matcher)
                            <li><a href="/connection/view/{{ $thePersonMatch->matchee }}">{{ getMatchName($thePersonMatch->matchee) }}</a></li>
                            <li>{{ getMatchTitle($thePersonMatch->matchee) }}</li>
                            <li>{{ getMatchCompany($thePersonMatch->matchee) }}</li>
                        @else
                            <li>{{ getMatchName($thePersonMatch->matcher) }}</li>
                            <li>{{ getMatchTitle($thePersonMatch->matcher) }}</li>
                            <li>{{ getMatchCompany($thePersonMatch->matcher) }}</li>
                        @endif
                        <li>{{ convertHTMLTime($thePersonMatch->date) }}</li>
                    </ul>
                @endforeach
            </div>
        </div>
        <div class="panel-heading">
            <h5>People <strong>{{ $matched["matchee"]["f_name"] }} {{ $matched["matchee"]["l_name"] }}</strong> Introduced Me To</h5>
        </div>
        <div class="panel-body">
            <div class="d-table">
                <ul class="d-column">
                  <li>Matched Me With</li>
                  <li>Title</li>
                  <li>Company</li>
                  <li>Date</li>
                </ul>
                @php
                    $personal_matches = \App\Match::where('author', $matched["matchee"]["id"])->where('personal', Auth::user()->id)->orderBy('id', 'DESC')->get();
                @endphp
                @foreach($personal_matches as $personal_match)
                    <ul class="d-row">
                        <li>
                            <a href="/connection/view/{{ $personal_match->matchee }}">{{ getMatchName($personal_match->matcher) }}</a>
                        </li>
                        <li>{{ getMatchTitle($personal_match->matcher) }}</li>
                        <li>{{ getMatchCompany($personal_match->matcher) }}</li>
                        <li>{{ convertHTMLTime($personal_match->date) }}</li>
                    </ul>
                @endforeach
            </div>


        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endif