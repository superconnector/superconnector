@extends('layouts.app')

@section('css')
    <style>
        .mce-notification-warning{
            display:none;
        }
    </style>
    <link rel="stylesheet" href="https://selectize.github.io/selectize.js/css/selectize.default.css" data-theme="default">
    <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">My Profile</div>
                <div class="panel-body">
                	@include('layouts.alerts')
                    <form method="POST" action="{{ URL('/profile') }}" class="form-horizontal">
                    	{{ CSRF_FIELD() }}
                    	<div class="form-group">
                       		<label for="f_name" class="col-md-12 text-left">First Name</label> 
                    		<div class="col-md-12">
                   				<input id="f_name" type="text" name="f_name" value="{{ Auth::user()->f_name }}" required="required" class="form-control">
                   			</div>
                    	</div>
	                    <div class="form-group">
	                     	<label for="l_name" class="col-md-12 text-left">Last Name</label>
	                     	<div class="col-md-12">
	                    		<input id="l_name" type="text" name="l_name" value="{{ Auth::user()->l_name }}" required="required" class="form-control">
	                    	</div>
	                    </div>
                    	<div class="form-group">
                    		<label for="email" class="col-md-12 text-left">E-Mail Address</label>
                    		<div class="col-md-12">
                    			<input id="email" type="email" name="email" value="{{ Auth::user()->email }}" required="required" class="form-control">
                    		</div>
                    	</div>
                        <div class="form-group">
                            <label for="email" class="col-md-12 text-left">Phone Number</label>
                            <div class="col-md-12">
                                <input id="phone" type="text" name="phone" value="{{ Auth::user()->phone }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-12 text-left">Title</label>
                            <div class="col-md-12">
                                <select data-placeholder="Select Job Title" name="title" id="title" class="demo-default" required>
                                    <option>Select Job Title</option>
                                    @foreach($occupations as $occupation)
                                        <option value="{{ $occupation->id }}" @if($occupation->id == Auth::user()->title) selected="" @endif>{{ $occupation->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-12 text-left">Company</label>
                            <div class="col-md-12">
                                <input id="company" type="text" name="company" value="{{ Auth::user()->company }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-12 text-left">Custom Email Signatures (3 Max)</label>
                            @for($i = 0; $i < 3; $i++)
                                @php
                                    $signature = $signatures[$i] ?? false;
                                @endphp
                                <div class="col-md-12">
                                    @if($signature)
                                    <textarea name="signature[]" rows="10" class="form-control signature">{{ $signature->content }}</textarea>
                                    @else
                                        <textarea name="signature[]" rows="10" class="form-control signature"></textarea>
                                    @endif
                                    <br>
                                </div>
                            @endfor
                        </div>
                    	<div class="form-group">
                    		<label for="email" class="col-md-12 left">Leave password blank if you are not changing it</label>
                    	</div>
                    	<div class="form-group">
                    		<label for="password" class="col-md-12 text-left">Password</label>
                    		<div class="col-md-12">
                    			<input type="password" id="password" class="hide">
                    			<input type="password" name="password" class="form-control" value="">
                    		</div>
                    	</div>
	                    <div class="form-group">
	                     	<div class="col-md-12">
	                     		<button type="submit" class="btn btn-primary">Update Profile</button>
	                     	</div>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
 <script src="https://cdn.tiny.cloud/1/h5038yp1fjvr0a1uk6kledxt9lgvq207l43v08nqokxufexz/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

  <script>
  tinymce.init({
    selector: '.signature',
    insert_button_items: 'image link | inserttable | textcolor',

  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount linkchecker',
  toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    default_link_target: "_blank",
    style_formats: [    
        {title: 'black text', inline: 'span', styles: {color: '#000000'}},
        {title: 'black text', inline: 'p', styles: {color: '#000000'}}
    ]
  });
  $('#title').selectize({
            create: true,
            sortField: 'text'
        });
  </script>
@endsection
