@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        .form-group .value{
            /*display:none; */
        }
        .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: 1px solid #ccd0d2 !important;
            border-radius: 4px;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .hoveredit{
            border: none !important;
            width:100%;
        }
        .hoverhelp{
            display: none;
        }
        .hoveredit:hover ~ .hoverhelp{
            display: block;
            position: absolute;
            left:20px;
        }
        .fa-pencil{
            cursor: hover;
        }
        #occupations input{
            width: 100% !important;
            padding:5px 10px;
            border-radius: 3px;
        }
        .save{
            display: none;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <h2>{{ $users->count() }}</h2>
                    <h3>Total Accounts</h3>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <h2>{{ $connections->count() }}</h2>
                    <h3>Total Connections Uploaded</h3>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <h2>{{ $matches->count() }}</h2>
                    <h3>Total Matches Made</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <h3>Utilities</h3>
        <ul>
            <li><a href="{{ route('occupations') }}">See All Job Titles</a></li>
            <li><a href="{{ route('mergeOccupations') }}">Find & Replace</a></li>
        </ul>
    </div>
</div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function(){
        $('#occupations').DataTable();
        $("body").on("click", "i.fa-pencil", function(){
            console.log("hit");
            $(this).parent().parent().find("input").removeAttr("readonly");
            $(this).parent().parent().find("input").removeClass();
        })

        $("body").on("keyup", "input", function(){
            if($(this).val() != $(this).attr("datdata-original-value")){
                $(this).parent().parent().find(".save").show();
                console.log("new");
            } else {
                $(this).parent().parent().find(".save").hide();
            }
        });


        $("body").on("click", ".delete", function(){
            var occupation = $(this).parent().parent().find("input").attr("data-id");
            console.log(occupation);

            $.ajax({url: "/admin/occupations/remove/" + occupation, success: function(result){
                
            }});
            $(this).parent().parent().remove();
        });
        $("body").on("click", ".save", function(){
            var occupation = $(this).parent().parent().find("input").val();
            var id = $(this).parent().parent().find("input").attr("data-id");

            console.log(occupation);

            $.ajax({url: "/admin/occupations/rename/" + id + "/" + occupation, success: function(result){
                
            }});
            $(this).parent().parent().find("input").attr("readonly");
            $(this).parent().parent().find("input").attr("data-original-value", occupation);
            $(this).parent().parent().find("input").addClass("hoveredit");
            $(this).parent().parent().find(".save").hide();
        });
    });
    </script>
@endsection
