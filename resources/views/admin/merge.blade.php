@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        .form-group .value{
            /*display:none; */
        }
        .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: 1px solid #ccd0d2 !important;
            border-radius: 4px;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .hoveredit{
            border: none !important;
            width:100%;
        }
        .hoverhelp{
            display: none;
        }
        .hoveredit:hover ~ .hoverhelp{
            display: block;
            position: absolute;
            left:20px;
        }
        .fa-pencil{
            cursor: hover;
        }
        #occupations input{
            width: 100% !important;
            padding:5px 10px;
            border-radius: 3px;
        }
        .save{
            display: none;
        }
        .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: 1px solid #ccd0d2 !important;
            border-radius: 4px;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            text-align: left;
        }
        .fav a, .fav a:hover {
            text-decoration: none;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header{
            text-align:left;
        }
    </style>
    <link rel="stylesheet" href="https://selectize.github.io/selectize.js/css/selectize.default.css" data-theme="default">
@endsection

@section('content')
<div class="container">
    <form method="POST" action="{{ route('submitMerge') }}">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
        @include('layouts.alerts')
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Find all instances of
                </div>
                <div class="panel-body text-center">
                    <select class="demo-default" name="old" required="" id="old">
                        <option>Select Occupation</option>
                        @foreach($occupations as $occupation)
                            <option value="{{ $occupation->id }}">{{ $occupation->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                And replace it with
                </div>
                <div class="panel-body text-center">
                    <select class="demo-default" name="new" required="" id="new">
                        <option>Select Occupation</option>
                        @foreach($occupations as $occupation)
                            <option value="{{ $occupation->id }}">{{ $occupation->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <input type="submit" value="Process Find & Replace" class="btn btn-warning" style="color:black">
            <br>
            <p>This action cannot be undone. The old occupation value will also be removed from the system.</p>
        </div>
    </div>
    </form>
</div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
    <script>
        $(document).ready(function() {
            $('#old').selectize({
                create: false,
                sortField: 'text'
            });
            $('#new').selectize({
                create: false,
                sortField: 'text'
            });
        });
    </script>
@endsection
