@extends('layouts.app')

@section('css')
    
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        .fa-star{
            color:silver;
        }
        i.fa-star.active{
            color:goldenrod;
        }
        .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: 1px solid #ccd0d2 !important;
            border-radius: 4px;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .fav a, .fav a:hover {
            text-decoration: none;
        }
        .connection-potential{
            display: none;
            background: #5cb85c;
            padding: 3px 5px;
            text-align: center;
            color: white;
            border-radius: 50%;
            font-size: 8px;
            box-shadow: 0px 0px 5px rgba(0,205,0, 0.7);
            position: absolute;
            margin-left: 5px;
            margin-top: 2px;
        }
    </style>
    <link rel="stylesheet" href="https://selectize.github.io/selectize.js/css/selectize.default.css" data-theme="default">
@endsection

@section('content')
<div class="container">
    <div class="row ">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a style="text-decoration: none;" data-toggle="collapse" href="#addContactForm" aria-expanded="false" aria-controls="addContactForm">Add New Contact <span class="caret"></span></a>
                    @include('layouts.alerts')
                    <div class="collapse multi-collapse row" id="addContactForm" style="padding-top:10px;">
                        <form method="POST" action="{{ URL('/connections') }}" id="addContact">
                            {{ CSRF_FIELD() }}
                            <div class="form-group col-md-6">
                                <input type="text" name="f_name" class="form-control" placeholder="First Name" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="l_name" class="form-control" placeholder="Last Name">
                            </div>
                            <div class="form-group col-md-12">
                            <select placeholder="Select Job Title" id="title" name="title" class="demo-default" required>
                                <option value=''>Select Job Title</option>
                                <?= $occupations; ?>
                            </select>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" name="company" class="form-control" placeholder="Company">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" name="email" class="form-control" placeholder="Email" required="">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" name="cell" class="form-control" placeholder="Cell #">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="text" name="work" class="form-control" placeholder="Work #">
                            </div>
                            <div class="form-group col-md-12">
                                <label>This person wants to connect with:</label>
                                <select id="looking" class="demo-default" placeholder="Select a person..." name="looking[]" multiple="">
                                <option>Select Job Titles</option>
                                <?= $occupations; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="submit" name="search" class="btn btn-primary" value="Add Contact">
                            </div>

                            <hr></hr>
                            
                        </form>
                        <form method="POST" action="{{ URL('/importContacts') }}" id="importContacts" enctype="multipart/form-data">
                            <input type="hidden" id="CSRF_FIELD" name="_token" value="" >
                            <div class="form-group col-md-12">
                                <h3>OR</h3>
                                <p>Upload new connections into your <strong>contacts</strong> with our CSV import tool. Upload your completed <a href="{{ URL('/import-sample.csv') }}"><span class="label label-primary">example template</span></a> below.</p>
                            </div>
                            <div class="form-group col-md-4">
                                <input type="file" name="top30" class="form-control" required="">
                            </div>
                            <div class="form-group col-md-4">
                                <input type="submit" name="submit" class="btn btn-success" value="Import Contacts">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">All Contacts</div>

                <div class="panel-body">
                    <table id="connections" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>First</th>
                                <th>Last</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Contact</th>
                                <th>Looking For</th>
                                <th>Sort</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($connections as $connection)
                                <tr class="connection-row" data-connection-id="{{ $connection->id }}">
                                    <td>{{ $connection->f_name }}</td>
                                    <td>{{ $connection->l_name }} <span class="connection-potential"></span></td>
                                    <td>{{ getOccupationTitle($connection->title) }}</td>
                                    <td>{{ $connection->company }}</td>
                                    <td>
                                        <ul>
                                            <li>@if(!empty($connection->email)) E: {{ $connection->email }} @endif</li>
                                            <li>@if(!empty($connection->cell)) C: {{ $connection->cell }} @endif</li>
                                            <li>@if(!empty($connection->work)) W: {{ $connection->work }} @endif</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                        @foreach(explode(",", $connection->looking) as $looking)
                                            @php $occupationtitle = getOccupationTitle($looking); @endphp
                                            @if(!empty($occupationtitle))
                                            <li>
                                                {{ $occupationtitle }}
                                            </li>
                                            @endif
                                        @endforeach
                                        </ul>
                                    </td>
                                    <td width="10%">
                                        @if($connection->favorite == 1) <span style="display: none;">1</span> @endif
                                        <ul>
                                            <li class="text-center fav" title="@if($connection->favorite == 1) Remove from Top Connection @else Add to Top Connection @endif"><a href="javascript:;" class="favorite" style="text-decoration: none !important;" data-connection-id="{{ $connection->id }}"><i style="font-size: 20px;" class="fave fa fa-star @if($connection->favorite == 1)active @endif"></i> Top</a></li>
                                            <li class="text-center"><a href="mailto:{{ $connection->email }}"><i class="fa fa-envelope"></i></a></li>
                                            <li class="text-center"><a href="{{ URL('/connection/view/' . $connection->id) }}"><i class="fa fa-pencil"></i></a></li>
                                            <li class="text-center"><a href="#" class="removeConnection" data-connection-id="{{ $connection->id }}"><i class="fa fa-trash"></i></a></li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
    <script>
        $(document).ready(function() {
            $('#connections').DataTable();

            $("body").on("click", ".removeConnection", function(){
                if(confirm("Are you sure you want to delete this contact?")){
                    $connectionID = $(this).attr("data-connection-id");
                    $.ajax({
                      url: '/connection/remove/' + $connectionID
                   });
                    $(this).parent().parent().parent().parent().remove();
                    $('#connections').DataTable().reload();
                }
            });

            $("body").on("click", ".favorite", function(){
                $connectionID = $(this).attr("data-connection-id");
                var favorite = $(this).parent().find(".fave");
                $.ajax({
                    context: this,
                    url: '/connection/favorite/' + $connectionID
               }).done(function( data ) {
                if(data == 30){
                    alert("You already have the maximum allowed number of 30 favorite connections.");
                } 
                else{
                    favorite.toggleClass("active");
                }
                    
                });
                
                
            });

        });
        $("#addContact").fadeTo( "fast", 0.33 );
        setTimeout(function(){
        
        $('#title').selectize({
            create: true,
            sortField: 'text',
            currentValue: '{{ Auth::user()->title }}'
        });

        $('#looking').selectize({
            theme: 'links',
            create: true,
            sortField: 'text'
        });
        
        $("#addContact").fadeTo( "fast", 1 );
}, 1000);

        $("#CSRF_FIELD").val($("#addContact input:eq(0)").val());

        $(".connection-row").each(function(){
                var id = $(this).attr("data-connection-id");
                var target = $(this).find(".connection-potential");
                $.ajax({
                    url: '/connection/verify/potential/' + id,
                    success: function(data){
                        if(data > 0){
                            target.show();
                            target.css("display", "inline")
                            target.html(data);
                        }
                    }
                });
            });
        
    </script>
@endsection
