<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <title>Super Connector Email Introduction</title>
</head>
<body>
<p>{{ $matcher->f_name }},</p>
<p>
I hope business has been treating you well. Last time we connected, you said that you would be able to introduce me to {{ getMatchName($matchee->id) }} from {{ $matchee->company }}. Do you mind making the introduction soon? I'd really appreciate it!</p>
<p>
All the best,
<br>
-{{ $myname }}
</p>
</body>
</html>