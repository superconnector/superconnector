@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        tr.danger{
            border: 1px solid red;
        }

        #skip-table, #skip-table tr, #skip-table tr td{
            background: transparent;
        }
        .rotating{
            -webkit-animation: rotation 1s infinite linear;
        }
        .odd, .even{
            transition: 1s;
        }
        .connection-potential{
            display: none;
            padding: 3px 5px;
            text-align: center;
            font-size: 20px;
            cursor: pointer;
            color:green;
        }

        @-webkit-keyframes rotation {
                from {
                        -webkit-transform: rotate(0deg);
                }
                to {
                        -webkit-transform: rotate(359deg);
                }
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Contacts With Opportunities for You</div>
                <div class="panel-body">
                    <table id="connections" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Contact</th>
                                <th class="text-center">Opportunities</th>
                                <th>Looking For</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($connections as $connection)
                                <tr class="connection-row" data-connection-id="{{ $connection->id }}">
                                    <td>
                                        {{ $connection->f_name }} {{ $connection->l_name }}<br>
                                        {{ getOccupationTitle($connection->title) }} <br>
                                        {{ $connection->company }} <br>
                                        <ul>
                                            <li>@if(!empty($connection->email)) E: {{ $connection->email }} @endif</li>
                                            <li>@if(!empty($connection->cell)) C: {{ $connection->cell }} @endif</li>
                                            <li>@if(!empty($connection->work)) W: {{ $connection->work }} @endif</li>
                                        </ul>
                                    </td>
                                    <td class="text-center"><span class="connection-potential" onclick="window.location.href='{{ URL('/connection/view/' . $connection->id) }}'"></span></td>
                                    <td>
                                        <ul>
                                        @foreach(explode(",", $connection->looking) as $looking)
                                            @php $occupationtitle = getOccupationTitle($looking); @endphp
                                            @if(!empty($occupationtitle))
                                            <li>
                                                {{ $occupationtitle }}
                                            </li>
                                            @endif
                                        @endforeach
                                        </ul>
                                    </td>
                                    <td width="10%">
                                        <ul>
                                            <li class="text-center fav" title="@if($connection->favorite == 1) Remove from Top Connection @else Add to Top Connection @endif"><a href="javascript:;" class="favorite" style="text-decoration: none !important;" data-connection-id="{{ $connection->id }}"><i style="font-size: 20px;" class="fave fa fa-star @if($connection->favorite == 1)active @endif"></i> Top</a></li>
                                            <li class="text-center"><a href="mailto:{{ $connection->email }}"><i class="fa fa-envelope"></i></a></li>
                                            <li class="text-center"><a href="{{ URL('/connection/view/' . $connection->id) }}"><i class="fa fa-pencil"></i></a></li>
                                            <li class="text-center"><a href="#" class="removeConnection" data-connection-id="{{ $connection->id }}"><i class="fa fa-trash"></i></a></li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(".connection-row").each(function(){
                var id = $(this).attr("data-connection-id");
                var target = $(this);
                $.ajax({
                    url: '/connection/verify/potential/' + id,
                    success: function(data){
                        if(data > 0){
                            target.find(".connection-potential").show();
                            target.find(".connection-potential").html(data);
                        }
                    }
                });

            });
        
        $(document).ready(function() {
            

            $(".removeConnection").click(function(){
                $connectionID = $(this).attr("data-connection-id");
                $.ajax({
                  url: '/connection/remove/' + $connectionID
               });
                $(this).parent().parent().parent().parent().remove();
                $('#connections').DataTable().reload();
            });

            $("body").on("click", ".refresh-top30", function(){
                $matcher = $(this).attr("data-matcher");
                $matchee = $(this).attr("data-matchee");
                $target = $(this);
                $($target).find("i").addClass("rotating");
                $.ajax({
                    dataType: 'json',
                    url: '/connection/refresh/' + $matcher + '/' + $matchee,
                    success: function(data){

                        if(data.error){
                            alert("No additional matches can be found at this time.");
                            
                            $($target).addClass("btn-danger");
                        } else{
                            $($target).removeClass("btn-danger");
                        }

                        $("#connectionName" + $matcher).text(data.connectionName);
                        $("#connectionTitle" + $matcher).text(data.connectionTitle);
                        $("#lookingFor" + $matcher).text(data.lookingFor);
                        $("#connectionOpp" + $matcher).html(data.connectionOpp);
                        $("#relevance" + $matcher).text(data.relevance);

                        $("#matcheeLink" + $matcher).attr("href", "/connection/view/" + $matchee);

                        $("#refresh" + $matcher).attr("data-matcher", data.matcher);
                        $("#refresh" + $matcher).attr("data-matchee", data.matchee);
                        $($target).find("i").removeClass("rotating");
                    }
                });
            });

            $("body").on("click", ".skipButton", function(){
                $matcher = $(this).attr("data-matcher");
                $skip = $(this).parent().parent().find(".skip-until").val();
                $target = $(this);
                if($skip != ""){
                    $.ajax({
                        dataType: 'json',
                        url: '/connection/skip/' + $matcher + "/" + $skip,
                        success: function(data){
                            
                        }
                    });
                    $(this).parent().parent().parent().parent().parent().parent().hide();
                }
            });

            

            setTimeout(function(){ 

                $('#connections').DataTable();
                $('#connections').DataTable().reload();

             }, 1000);

            
        });
    </script>
@endsection