@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        .mce-notification-warning, .hide{
            display:none;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Introduce {{ $matcher->f_name . " (" . getOccupation($matcher->title)  . ")"}} and {{ $matchee->f_name . " (" . getOccupation($matchee->title)  . ")"}}
                </div>
                <div class="panel-body">
                    @include('layouts.alerts')
                    <form method="POST" action="{{ URL('/match/' . $matcher->id . '/' . $matchee->id) }}">
                        {{ CSRF_FIELD() }}
                        @if($top30)<input type="hidden" value="top30" name="top30">@endif
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>From:</label>
                                <input type="email" class="form-control" name="from" value="{{ Auth::user()->email }}" required="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>My First Name:</label>
                                <input type="text" class="form-control" name="from_fname" value="{{ Auth::user()->f_name }}" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label>My Last Name:</label>
                                <input type="text" class="form-control" name="from_lname" value="{{ Auth::user()->l_name }}" required="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Step 1: Select Template</label>
                                <select class="form-control" name="template" id="templates" data-matcher="{{ $matcher->id }}" data-matchee="{{ $matchee->id }}">
                                    <option value="0">No Template</option>
                                @foreach($templates as $template)
                                    <option value="{{ $template->id }}">{{ $template->name }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label>Step 2: Subject</label>
                                <input type="text" name="subject" id="subject" class="form-control" required="" value="{{ $matcher->f_name }}, I'd like to Introduce You to {{ $matchee->f_name . ' ' . $matchee->l_name . ' ' . getOccupationGrammer($matchee->title) }}">
                                
                            </div>
                            <div class="form-group col-md-12">
                                <label>Step 3: Email Message</label>
                                <textarea id="message" name="content" class="form-control" rows="7" required="" placeholder="Tax Attorney, Estate Planner, Family Attorney">{{ $matcher->f_name }}, when we last spoke, you mentioned that you were interested in meeting someone like {{ $matchee->f_name }}, {{ getOccupationGrammer($matchee->title) }}. The good news is that {{ $matchee->f_name }} has told me that they were looking for someone like you. I figured getting you two introduced would be mutually beneficial for the both of you.</textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label>Step 4: Your Email Signature</label>
                                <select name="version" class="form-control" id="version">
                                    @foreach($signatures as $signature)
                                        <option value="{{ $signature->version }}">Version {{ $signature->version + 1 }}</option>
                                    @endforeach
                                </select>
                                @for($i = 0; $i < sizeof($signatures); $i++)
                                    <div id="signature-{{ $signatures[$i]->version }}" class="previews" style="padding:10px; border:1px solid black; border-radius: 0.325em; margin-top:10px;">
                                        <p >{!! $signatures[$i]->content !!}</p>
                                    </div>
                                @endfor
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" name="sendtest" class="btn btn-primary" value="Send Test to Myself">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" name="search" class="btn btn-success" value="Send Introduction">
                            </div>
                        <div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://cdn.tiny.cloud/1/h5038yp1fjvr0a1uk6kledxt9lgvq207l43v08nqokxufexz/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        $(document).ready(function() {
            $("#templates").on("change", function(){
                $templateID = $(this).val();
                $matchee = $(this).attr("data-matchee");
                $matcher = $(this).attr("data-matcher");
                var editor = tinymce.get('message'); // use your own editor id here - equals the id of your textarea
                var content = editor.getContent();
                content = content.replace(/{\$baseurl}/g, 'http://localhost');
                
                if($templateID > 0){
                    $.ajax({
                      url: '/templates/data/' + $templateID + '/' + $matcher + '/' + $matchee
                   }).done( function(data){
                        $("#tinymce").html(data);
                        $("#subject").val(data['subject'])
                        editor.setContent(data['content']);
                    });
                } else {
                    editor.setContent("");
                }
            });

            $("#signature-1").hide();
            $("#signature-2").hide();

            $("#version").on("change", function(){
                var version = $(this).val();

                $(".previews").each(function(){
                    $(this).hide();
                })

                $("#signature-" + version).show();
            });
        });
    </script>
    <script>
        tinymce.init({
            selector: '.signature',
            insert_button_items: 'image link | inserttable | textcolor',

              plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount linkchecker',

          toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            default_link_target: "_blank",
            style_formats: [    
                {title: 'black text', inline: 'span', styles: {color: '#000000'}},
                {title: 'black text', inline: 'p', styles: {color: '#000000'}}
            ]
          });
        tinymce.init({
            selector: '#message',
            insert_button_items: 'image link | inserttable | textcolor',

              plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount linkchecker',

          toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            default_link_target: "_blank",
            style_formats: [    
                {title: 'black text', inline: 'span', styles: {color: '#000000'}},
                {title: 'black text', inline: 'p', styles: {color: '#000000'}}
            ]
          });
    </script>
@endsection
