@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-body">
                    <h1>Success!</h1>
                    <h3>You introduced {{ $matcher->f_name . " (" . getOccupationTitle($matcher->title)  . ")"}} to {{ $matchee->f_name . " (" . getOccupationTitle($matchee->title)  . ")"}}</h3>
                    <h4>Here's the email you sent</h4><br>
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#connections').DataTable();

            $(".removeConnection").click(function(){
                $connectionID = $(this).attr("data-connection-id");
                $.ajax({
                  url: '/connection/remove/' + $connectionID
               });
                $(this).parent().parent().parent().parent().remove();
                $('#connections').DataTable().reload();
            });
        } );
    </script>
@endsection
