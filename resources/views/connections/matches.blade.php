@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: 1px solid #ccd0d2 !important;
            border-radius: 4px;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .fav a, .fav a:hover {
            text-decoration: none;
        }
        .d-table {
            width: 100%;
  min-width: 300px;
  min-height: 100px;
  display: block;
  border: 1px solid whitesmoke;
  padding:10px;
  margin: 10px 0;
}

.d-table ul {
  list-style-type: none;
  margin:0;
  padding:0;
}

ul.d-column li {
  padding: 0px;
  display: inline-block;
  width: 24.5%;
  height: 30px;
  margin:0;
  font-size: 16px;
  font-weight: bold;
}

.d-row li {
  padding: 0px;
  display: inline-block !important;
  width: 24.5%;
  height: 20px;
  margin:0;
  font-size: 12px;
}
    </style>
    <link rel="stylesheet" href="https://selectize.github.io/selectize.js/css/selectize.default.css" data-theme="default">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">My Contacts</div>
                <div class="panel-body">
                    <select id="looking" class="demo-default" placeholder="Select a person..." name="looking[]">
                        @foreach($connections as $connection)
                            <option value="{{ $connection->id }}" @if($connection->id == $contact) selected @endif>{{ getMatchName($connection->id) }}, {{ getOccupation($connection->title) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">All Matches</div>
                <div class="panel-body">
                    <table id="connections" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Your Contact</th>
                                <th>Looking For</th>
                                <th>Can Match With</th>
                                <th>Relevance</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($matches as $match)
                                <?php 

                                    if(empty($match[1])){
                                        dd($match);
                                    }
                                    $matched = displayMatch($match[0], $match[1]); 
                                    $matchmade = false;
                                    if(hasMatchMailSent($matched["matcher"]["id"], $matched["matchee"]["id"])){
                                        $matchmade = true;
                                    }
                                    $i = rand(100000, 999999);
                                ?>
                                @if($match[4] != 8920)
                                    <tr data-score="{{ $match[5] }}">
                                        <td><strong><a href="/connection/view/{{ $matched['matcher']['id'] }}" target="_blank">{{ $matched["matcher"]["f_name"] . " " . $matched["matcher"]["l_name"] }}</a> ({{ getOccupation($matched["matcher"]["title"]) }})</strong></td>
                                        <td><br>
                                            @php
                                                $theperson = \App\Connection::find($match[0]);
                                            @endphp
                                            @foreach(explode(",", $theperson->looking) as $key => $lookingfor)
                                                @php $occuptationTitle = getOccupation($lookingfor); @endphp
                                                @if(getOccupation($lookingfor) == getOccupation($match[4]))<strong style="text-decoration: underline;">{{ getOccupation($match[4]) }}</strong>@else @if(!empty($occuptationTitle))<span style="color:silver;">{{ $occuptationTitle }}</span>@endif
                                                @endif @if($key < sizeof(explode(",", $theperson->looking)) -1 && $lookingfor != 8920 && !empty($occuptationTitle)),@endif
                                            @endforeach
                                        </td>
                                        <td><strong><a href="#" data-toggle="modal" data-target="#preview{{ $i }}">{{ $matched["matchee"]["f_name"] . " " . $matched["matchee"]["l_name"] }}</a> ({{ getOccupation($matched["matchee"]["title"]) }})</strong></td>
                                            <td class="text-center">{{ $match[3] }}</td>
                                            <td class="text-center">@if($matchmade) ✓ @endif</td>
                                            <td class="text-center"><a href='/match/{{ $matched["matcher"]["id"] }}/{{ $matched["matchee"]["id"] }}' target='_blank'>Introduce @if($matchmade) Again @endif <i class="fa fa-envelope"></i></a></td>
                                    </tr>
                                    <div class="modals-move">@include('layouts.contact-preview', $matched)</div>
                                @endif
                                
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modals">
</div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
    <script>
        $(document).ready(function() {
            $(".modals-move").each(function(){
                $(this).appendTo("#modals");
            });
            

            $(".removeConnection").click(function(){
                $connectionID = $(this).attr("data-connection-id");
                $.ajax({
                  url: '/connection/remove/' + $connectionID
               });
                $(this).parent().parent().parent().parent().remove();
                $('#connections').DataTable().reload();
            });

            $('#looking').selectize({
                create: false,
                sortField: 'text'
            });

            $("#looking").on("change", function(){
                var id = $(this).val();
                if(id != ""){
                    window.location.href = "/matches/find/" + id;
                }
            });

            $('#connections').DataTable({paging: false}).unique();
        } );
    </script>
@endsection
