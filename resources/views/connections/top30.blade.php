@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        tr.danger{
            border: 1px solid red;
        }

        #skip-table, #skip-table tr, #skip-table tr td{
            background: transparent;
        }
        .rotating{
            -webkit-animation: rotation 1s infinite linear;
        }
        .odd, .even{
            transition: 1s;
        }
        .connection-potential{
            display: none;
            background: #5cb85c;
            padding: 3px 5px;
            text-align: center;
            color: white;
            border-radius: 50%;
            font-size: 8px;
            box-shadow: 0px 0px 5px rgba(0,205,0, 0.7);
            position: absolute;
            margin-left: 5px;
            margin-top: 2px;
        }

        @-webkit-keyframes rotation {
                from {
                        -webkit-transform: rotate(0deg);
                }
                to {
                        -webkit-transform: rotate(359deg);
                }
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Import</div>

                @if(session('success') !== null)
                    {!! session('success') !!}
                @endif

                <div class="panel-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('importTop30') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <p>Upload new connections into your <strong>Top Connections</strong> with our CSV import tool. Upload your completed <a href="{{ URL('/import-sample.csv') }}"><span class="label label-primary">example template</span></a> below.</p>
                        </div>
                        <div class="form-group">
                            <input type="file" name="top30" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success" value="Import Connections">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Tasks for Your Top Connections</div>
                <div class="panel-body">
                    <table id="connections" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Contact</th>
                                <th>Looking For</th>
                                <th style="max-width: 300px;">Match Them With</th>
                                <th>Relevance</th>
                                <th></th>
                                <th>Skip Until</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($connections as $connectionToMatch)
                                @php
                                $connection = \App\Connection::find($connectionToMatch[0]); 
                                $matchee = \App\Connection::find($connectionToMatch[1]); 
                                
                                @endphp
                                @if(!isset($connection->skip) || DateDifference($connection->skip, Date('Y-m-d')) <= 0)
                                @php
                                    if(Date('Y-m-d') == $connection->skip){
                                        $connection->update([
                                            "skip" => null
                                        ]);
                                    }
                                @endphp
                                <tr data-connection-id="{{ $connection->id }}" class="connection-row">
                                    <td><a href="/connection/view/{{ $connection->id }}" id="connectionLink{{ $connection->id }}" target="_blank" style="text-decoration: none;"><span id="connectionName{{ $connection->id }}">{{ $connection->f_name . " " . $connection->l_name }}</span> <span class="connection-potential"></span></a><br>
                                    <b id="connectionTitle{{ $connection->id }}">{{ getOccupationTitle($connection->title) }}</b></td>
                                    <td id="lookingFor{{ $connection->id }}">{{ getOccupationTitle($connectionToMatch[4]) }}</td>
                                    <td id="connectionOpp{{ $connection->id }}">
                                        <a href="/connection/view/{{ $matchee->id }}" id="matcheeLink{{ $connection->id }}" target="_blank">{{ $matchee->f_name . " " . $matchee->l_name }}</a> <br>
                                            <b>{{ getOccupationTitle($matchee->title) }}</b> <br>
                                            <a href='/matchtop30/{{ $connection->id }}/{{ $matchee->id }}' target='_blank'>Finish Introduction <i class="fa fa-envelope"></i></a></a>
                                    </td>
                                    <td class="text-center" id="relevance{{ $connection->id }}">{{ $connectionToMatch[3] }}</td>
                                    <td class="text-center"><button type="button" if="refresh{{ $connection->id }}" class="refresh-top30 btn" data-matcher="{{ $connectionToMatch[0] }}" data-matchee="{{ $connectionToMatch[1] }}"><i class="fa fa-refresh"></button></td>
                                    <td class="text-center">
                                        <table id="skip-table" style="background: none;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="date" class="form-control skip-until"  style="max-width: 160px;" min="{{ Date('Y-m-d') }}"></td>
                                                    <td><button class="btn btn-success skipButton" data-matcher="{{ $connectionToMatch[0] }}">Skip</button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#connections').DataTable();

            $(".removeConnection").click(function(){
                $connectionID = $(this).attr("data-connection-id");
                $.ajax({
                  url: '/connection/remove/' + $connectionID
               });
                $(this).parent().parent().parent().parent().remove();
                $('#connections').DataTable().reload();
            });

            $("body").on("click", ".refresh-top30", function(){
                $matcher = $(this).attr("data-matcher");
                $matchee = $(this).attr("data-matchee");
                $target = $(this);
                $($target).find("i").addClass("rotating");
                $.ajax({
                    dataType: 'json',
                    url: '/connection/refresh/' + $matcher + '/' + $matchee,
                    success: function(data){

                        if(data.error){
                            alert("No additional matches can be found at this time.");
                            
                            $($target).addClass("btn-danger");
                        } else{
                            $($target).removeClass("btn-danger");
                        }

                        $("#connectionName" + $matcher).text(data.connectionName);
                        $("#connectionTitle" + $matcher).text(data.connectionTitle);
                        $("#lookingFor" + $matcher).text(data.lookingFor);
                        $("#connectionOpp" + $matcher).html(data.connectionOpp);
                        $("#relevance" + $matcher).text(data.relevance);

                        $("#matcheeLink" + $matcher).attr("href", "/connection/view/" + $matchee);

                        $("#refresh" + $matcher).attr("data-matcher", data.matcher);
                        $("#refresh" + $matcher).attr("data-matchee", data.matchee);
                        $($target).find("i").removeClass("rotating");
                    }
                });
            });

            $("body").on("click", ".skipButton", function(){
                $matcher = $(this).attr("data-matcher");
                $skip = $(this).parent().parent().find(".skip-until").val();
                $target = $(this);
                if($skip != ""){
                    $.ajax({
                        dataType: 'json',
                        url: '/connection/skip/' + $matcher + "/" + $skip,
                        success: function(data){
                            
                        }
                    });
                    $(this).parent().parent().parent().parent().parent().parent().hide();
                }
            });

            $(".connection-row").each(function(){
                var id = $(this).attr("data-connection-id");
                var target = $(this);
                $.ajax({
                    url: '/connection/verify/potential/' + id,
                    success: function(data){
                        if(data > 0){
                            target.find(".connection-potential").show();
                            target.find(".connection-potential").html(data);
                        }
                    }
                });
            });
        });
    </script>
@endsection