@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        .form-group .value{
            /*display:none; */
        }
        .selectize-control.single .selectize-input{
            background-color: white !important;
            background-image: none !important;
            border: 1px solid #ccd0d2 !important;
            border-radius: 4px;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

    </style>
        <link rel="stylesheet" href="https://selectize.github.io/selectize.js/css/selectize.default.css" data-theme="default">

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Contact
                </div>
                <div class="panel-body">
                    @include('layouts.alerts')
                        <form method="POST" action="{{ URL('/connection/' . $connection->id) }}">
                            {{ CSRF_FIELD() }}
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <input type="text" name="f_name" class="form-control" value="{{ $connection->f_name }}" placeholder="First Name" required="">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Last Name</label>
                                <input type="text" name="l_name" class="form-control" value="{{ $connection->l_name }}" placeholder="Last Name">
                            </div>
                            <div class="form-group col-md-12">
                                <label>Job Title</label>
                                <select data-placeholder="Select Job Title" id="mytitle" name="title" class="demo-default" required>
                                <option>Select Job Title</option>
                                @foreach($occupations as $occupation)
                                    <option value="{{ $occupation->id }}" @if($occupation->id == $connection->title) selected @endif>{{ $occupation->title }}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label>Company</label>
                                <input type="text" name="company" class="form-control" value="{{ $connection->company }}" placeholder="Company">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" value="{{ $connection->email }}" placeholder="Email" required="">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Cell</label>
                                <input type="text" name="cell" class="form-control" value="{{ $connection->cell }}" placeholder="Cell #">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Work</label>
                                <input type="text" name="work" class="form-control" value="{{ $connection->work }}" placeholder="Work #">
                            </div>
                            <div class="form-group col-md-12">
                                <label>This person wants to connect with:</label>
                                <select id="looking" class="demo-default" placeholder="Select one or more titles" name="looking[]" multiple="">
                                <option>Select Job Titles</option>
                                @foreach($occupations as $occupation)
                                    <option value="{{ $occupation->id }}" 
                                    <?php 
                                        $looking = explode(",", $connection->looking);
                                        if(in_array($occupation->id, $looking)){
                                            echo "selected";
                                        }
                                    ?>>
                                    {{ $occupation->title }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label>The ways this person could become a client are:</label>
                                <select id="opportunities" class="demo-default" placeholder="Select one or more opportunities" name="opportunities[]" multiple="">
                                @foreach($opportunities as $opportunity)
                                    <option value="{{ $opportunity->id }}" 
                                    <?php 
                                        $looking = explode(",", $connection->opportunities);
                                        if(in_array($opportunity->id, $looking)){
                                            echo "selected";
                                        }
                                    ?>>
                                    {{ $opportunity->title }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="submit" name="search" class="btn btn-primary" value="Save Contact">
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    I Matched <strong>{{ $connection->f_name }} {{ $connection->l_name }}</strong>
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('manual-match') }}">
                        {{ CSRF_FIELD() }}
                        <input type="hidden" name="matcher" value="{{ $connection->id }}">
                        <div class="form-group">
                            <label>With</label>
                            <select id="matchee" class="demo-default" placeholder="Select Connection" name="matchee">
                                @foreach($matchees as $matchee)
                                    <option value="{{ $matchee->id }}">{{ getMatchName($matchee->id) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>On</label>
                            <input type="date" name="date" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label>Because</label>
                            <input type="text" name="notes" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success" value="Save Match">
                        </div>
                        <!-- In case the contact does not exist-->
                        <div id="newmatchforthem" class="modal" tabindex="-1" role="dialog">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Please Basic Contact Info for <span class="newmatchforthem-name"></span></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <div class="form-group">
                                    <p>We could not find <span class="newmatchforthem-name"></span> in your contacts. To record this match, please provide the following info. You can edit this contact later.</p>
                                </div>
                                <div class="form-group">
                                    <label>Please Enter Their Email:</label>
                                    <input type="email" name="newmatchemail" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Please Enter Their Company:</label>
                                    <input type="text" name="newmatchcompany" class="form-control">
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Save & Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>{{ $connection->f_name }} {{ $connection->l_name }}</strong> Matched Me
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route('manual-personal-match') }}">
                        {{ CSRF_FIELD() }}
                        <input type="hidden" name="author" value="{{ $connection->id }}">
                        <div class="form-group">
                            <label class="form-label">With</label>
                            <select id="matcherr" class="demo-default" placeholder="Select Connection" name="matcher">
                                @foreach($matchers as $matcher)
                                    <option value="{{ $matcher->id }}">{{ getMatchName($matcher->id) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">On</label>
                            <input type="date" name="date" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Because</label>
                            <input type="text" name="notes" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success" value="Save Match">
                        </div>
                        <!-- In case the contact does not exist-->
                        <div id="newmatchfromthem" class="modal" tabindex="-1" role="dialog">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Please Basic Contact Info for <span class="newmatchfromthem-name"></span></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <div class="form-group">
                                    <p>We could not find <span class="newmatchfromthem-name"></span> in your contacts. To record this match, please provide the following info. You can edit this contact later.</p>
                                </div>
                                <div class="form-group">
                                    <label>Please Enter Their Email:</label>
                                    <input type="email" name="newmatchemail" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Please Enter Their Company:</label>
                                    <input type="text" name="newmatchcompany" class="form-control">
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Save & Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Matches I've Made for <strong>{{ $connection->f_name }} {{ $connection->l_name }}</strong>
                </div>
                <div class="panel-body">
                    <table id="connections" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Matched with</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Notes</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($matches as $match)
                                @if(is_object(\App\Connection::find($match->matchee)))
                                <tr>
                                    @if($connection->id == $match->matcher)
                                        <td><a href="/connection/view/{{ $match->matchee }}">{{ getMatchName($match->matchee) }}</a></td>
                                        <td>{{ getMatchTitle($match->matchee) }}</td>
                                        <td>{{ getMatchCompany($match->matchee) }}</td>
                                    @else
                                        <td>{{ getMatchName($match->matcher) }}</td>
                                        <td>{{ getMatchTitle($match->matcher) }}</td>
                                        <td>{{ getMatchCompany($match->matcher) }}</td>
                                    @endif
                                    <td>{{ $match->notes }}</td>
                                    <td>{{ convertHTMLTime($match->date) }}</td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="display: none;">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    People <strong>{{ $connection->f_name }} {{ $connection->l_name }}</strong> Introduced Me To
                </div>
                <div class="panel-body">
                    <table id="personalConnections" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Matched Me With</th>
                                <th>Title</th>
                                <th>Company</th>
                                <th>Notes</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($personal_matches as $personal_match)
                                <tr>
                                    <td>
                                        <a href="/connection/view/{{ $personal_match->matchee }}">{{ getMatchName($personal_match->matcher) }}</a>
                                    </td>
                                    <td>{{ getMatchTitle($personal_match->matcher) }}</td>
                                    <td>{{ getMatchCompany($personal_match->matcher) }}</td>
                                    <td>{{ $personal_match->notes }}</td>
                                    <td>{{ convertHTMLTime($personal_match->date) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>{{ $connection->f_name }} {{ $connection->l_name }}</strong> Has Yet to Introduce Me To:
                </div>
                <div class="panel-body">
                    <table id="potentialconnections" class="display" cellspacing="0" width="100%">
                        <thead>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th></th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach($potential_matches as $match)

                            <?php 
                            if(empty($match[1])){
                                dd($match);
                            }
                            $matched = displayMatch($match[0], $match[1]); 
                            $matchmade = false;
                            $matcheeData = \App\Connection::find($matched["matchee"]["id"]);
                            if(hasMatchMailSent($matched["matcher"]["id"], $matched["matchee"]["id"])){
                                $matchmade = true;
                            }
                            ?>
                                @if($match[4] != 8920 && !$matchmade)
                                <tr data-score="{{ $match[5] }}">
                                    <td><strong>{{ $matched["matchee"]["f_name"] . " " . $matched["matchee"]["l_name"] }}</strong>
                                    </td>
                                    <td>{{ getOccupation($matched["matchee"]["title"]) }} </td>
                                    <td>{{ $matcheeData->company }}</td>
                                    <td>{{ $matcheeData->email }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endif
                            @endforeach
                            @foreach($offline_matches as $match)
                                @php 
                                    $matcher = \App\Connection::find($match->matcher); 
                                    $matchee = \App\Connection::find($match->matchee); 
                                @endphp
                                @if(is_object($matchee))
                                <tr data-score="{{ $match[5] }}">
                                    <td><strong>{{ $matchee->f_name . " " . $matchee->l_name }}</strong><br>
                                    <p>{{ $matchee->notes }}</p></td>
                                    <td>{{ getOccupation($matchee->title) }} </td>
                                    <td>{{ $matchee->company }}</td>
                                    <td>{{ $matchee->email }}</td>
                                    <td class="text-center">
                                    @if($match->status == 0)
                                    <button class="btn btn-default btn-sm mark-complete" data-match="{{ $match->id }}">Mark Complete</button>
                                    <br><p></p>
                                    @else
                                        <i class="fa fa-check"></i> Match Complete
                                    @endif
                                    </td>
                                    <td class="text-center">
                                    @if($match->status == 0)
                                        <button class="btn btn-info btn-sm send-reminder" data-match="{{ $match->id }}">Send Reminder</button><br>
                                    <small>Last Reminder: {{ convertHTMLTime($match->reminded_at) }}</small>
                                    @endif
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>{{ $connection->f_name }} {{ $connection->l_name }}</strong> said he would introduce me to:
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('manual-offline-match') }}">
                        {{ CSRF_FIELD() }}
                        <input type="hidden" name="matcher" value="{{ $connection->id }}">
                        <input type="hidden" name="matchee" value="0">
                        <div class="form-group">
                            <label class="form-label">Email</label>
                            <input type="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label">First Name</label>
                            <input type="text" name="f_name" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Last Name</label>
                            <input type="text" name="l_name" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Company</label>
                            <input type="text" name="company" class="form-control" required="">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Notes about Opportunity</label>
                            <textarea name="notes" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Cell #</label>
                            <input type="number" step="1" name="cell" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Work #</label>
                            <input type="number" step="1" name="work" class="form-control">
                        </div>

                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success" value="Save">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $('#mytitle').selectize({
            create: true,
            sortField: 'text',
            currentValue: '{{ Auth::user()->title }}'
        });
        $('#looking').selectize({
            theme: 'links',
            create: true,
            sortField: 'text'
        });
        $('#opportunities').selectize({
            theme: 'links',
            create: true,
            sortField: 'text'
        });
        $('#matcher').selectize({
            theme: 'links',
            create: true,
            sortField: 'text'
        });

        
        $('#matcherr').selectize({
            theme: 'links',
            create: true,
            sortField: 'text',
            onItemAdd: function(text) {
                if(!$.isNumeric(text)){
                $(".newmatchfromthem-name").each(function(){
                    $(this).text(text);
                });
                $("#newmatchfromthem").modal('toggle');
                }
            }
        });
        $('#matchee').selectize({
            theme: 'links',
            create: true,
            sortField: 'text',
            onItemAdd: function(text) {
                if(!$.isNumeric(text)){
                $(".newmatchforthem-name").each(function(){
                    $(this).text(text);
                });
                $("#newmatchforthem").modal('toggle');
                }
            }
        });
        $('#connections').DataTable().unique();
        $('#personalConnections').DataTable().unique();
        $('#potentialconnections').DataTable().unique();

        $("body").on("click", ".mark-complete", function(){
                var button = $(this);
                $matchID = $(this).attr("data-match");
                $.ajax({
                    context: this,
                    url: '/matches/manualOffline/finish/' + $matchID
               }).done(function( data ) {
                    button.parent().html('<i class="fa fa-check"></i> Match Complete');
                    
                });        
                
            });

        $("body").on("click", ".send-reminder", function(){
                var button = $(this);
                var matchID = $(this).attr("data-match");
                $.ajax({
                    context: this,
                    url: '/matches/manualOffline/remind/' + matchID
               }).done(function( data ) {
                    alert("A reminder has been sent!");
                });        
                
            });
    </script>
@endsection
