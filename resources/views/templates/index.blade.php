@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        .mce-notification-warning{
            display:none;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a style="text-decoration: none;" data-toggle="collapse" href="#addContactForm" aria-expanded="false" aria-controls="addContactForm">Add New Email Template <span class="caret"></span></a>
                    @include('layouts.alerts')
                    <div class="collapse multi-collapse row" id="addContactForm" style="padding-top:10px;">
                        <form method="POST" action="{{ URL('/templates') }}">
                            {{ CSRF_FIELD() }}
                            <div class="form-group col-md-12">
                                <input type="text" name="name" class="form-control" placeholder="Template Name" required="">
                            </div>
                            <div class="form-group col-md-12">
                                <label>Use the "Template Variables" dropdown to select the following variables:</label>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>This Person:</th>
                                            <th>Is Being Introduced To:</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><label class="label label-primary">[FIRST_NAME]</label></td>
                                            <td><label class="label label-danger">[FIRST_NAME2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[LAST_NAME]</label></td>
                                            <td><label class="label label-danger">[LAST_NAME2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[TITLE]</label></td>
                                            <td><label class="label label-danger">[TITLE2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[COMPANY]</label></td>
                                            <td><label class="label label-danger">[COMPANY2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[CELL_PHONE]</label></td>
                                            <td><label class="label label-danger">[CELL_PHONE2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[WORK_PHONE]</label></td>
                                            <td><label class="label label-danger">[WORK_PHONE2]</label></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Email Subject:</label>
                                <textarea id="newsubject" class="form-control" name="subject" placeholder=""></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="form-label">Email Body:</label>
                                <textarea id="newtemplate" class="form-control" name="content" placeholder="Tax Attorney, Estate Planner, Family Attorney"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="submit" name="search" class="btn btn-primary" value="Add Template">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">All Templates</div>

                <div class="panel-body">
                    <table id="templates" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Template Name</th>
                                <th>Subject</th>
                                <th>Created</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($templates as $template)
                                <tr>
                                    <td width="20%">{{ $template->name }}</td>
                                    <td width="50%">{!! $template->subject !!}</td>
                                    <td>{{ convertTimestamp($template->date) }}</td>
                                    <td class="text-center">
                                        <ul>
                                            <li><a href="{{ URL('/templates/view/' . $template->id) }}" target="_blank"><i class="fa fa-eye"></i></a></li>
                                            <li><a href="#" class="removeTemplate" data-template-id="{{ $template->id }}"><i class="fa fa-trash"></i></a></li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.tiny.cloud/1/h5038yp1fjvr0a1uk6kledxt9lgvq207l43v08nqokxufexz/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        $(document).ready(function() {
            $('#templates').DataTable();

            $(".removeTemplate").click(function(){
                $templateID = $(this).attr("data-template-id");
                $.ajax({
                  url: '/templates/delete/' + $templateID
               });
                $(this).parent().parent().parent().parent().remove();
                $('#templates').DataTable().reload();
            });
        } );
    </script>
      <script>


  tinymce.init({
    selector: '#newtemplate',
    insert_button_items: 'image link | inserttable | textcolor',

    menubar: false,
  plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount linkchecker',
  toolbar: 'mybutton',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | mybutton',
    default_link_target: "_blank",
    style_formats: [    
        {title: 'black text', inline: 'span', styles: {color: '#000000'}},
        {title: 'black text', inline: 'p', styles: {color: '#000000'}}
    ],
    setup: function(editor) {
    editor.addButton('mybutton', {
      type: 'menubutton',
      text: 'Template Variables',
      icon: false,
      menu: [{
            text: 'Introduce Person One',
            context: 'tools',
            menu:[
                {
                    text: 'First Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[FIRST_NAME]</span>&nbsp;');
                      }
                },
                {
                    text: 'Last Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[LAST_NAME]</span>&nbsp;');
                      }
                },
                {
                    text: 'Title',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[TITLE]</span>&nbsp;');
                      }
                },
                {
                    text: 'Company',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[COMPANY]</span>&nbsp;');
                      }
                },
                {
                    text: 'Cell Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[CELL_PHONE]</span>&nbsp;');
                      }
                },
                {
                    text: 'Work Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[WORK_PHONE]</span>&nbsp;');
                      }
                }
            ]
      }, {
        text: 'To Person Two',
            context: 'tools',
            menu:[
                {
                    text: 'First Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[FIRST_NAME2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Last Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[LAST_NAME2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Title',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[TITLE2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Company',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[COMPANY2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Cell Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[CELL_PHONE2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Work Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[WORK_PHONE2]</span>&nbsp;');
                      }
                }
            ]
      }]
    });
  }
  });
  tinymce.init({
    selector: '#newsubject',
    insert_button_items: 'image link | inserttable | textcolor',

    menubar: false,
    plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount linkchecker',
    toolbar: 'mybutton',
    toolbar1: 'removeformat | mybutton',
    default_link_target: "_blank",
    style_formats: [    
        {title: 'black text', inline: 'span', styles: {color: '#000000'}},
        {title: 'black text', inline: 'p', styles: {color: '#000000'}}
    ],
    setup: function(editor) {
    editor.addButton('mybutton', {
      type: 'menubutton',
      text: 'Template Variables',
      icon: false,
      menu: [{
            text: 'Introduce Person One',
            context: 'tools',
            menu:[
                {
                    text: 'First Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[FIRST_NAME]</span>&nbsp;');
                      }
                },
                {
                    text: 'Last Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[LAST_NAME]</span>&nbsp;');
                      }
                },
                {
                    text: 'Title',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[TITLE]</span>&nbsp;');
                      }
                },
                {
                    text: 'Company',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[COMPANY]</span>&nbsp;');
                      }
                },
                {
                    text: 'Cell Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[CELL_PHONE]</span>&nbsp;');
                      }
                },
                {
                    text: 'Work Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[WORK_PHONE]</span>&nbsp;');
                      }
                }
            ]
      }, {
        text: 'To Person Two',
            context: 'tools',
            menu:[
                {
                    text: 'First Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[FIRST_NAME2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Last Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[LAST_NAME2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Title',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[TITLE2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Company',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[COMPANY2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Cell Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[CELL_PHONE2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Work Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[WORK_PHONE2]</span>&nbsp;');
                      }
                }
            ]
      }]
    });
  }
  });
  </script>
@endsection
