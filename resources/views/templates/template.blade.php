@extends('layouts.app')

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        th, td{
            text-align:left;
        }
        table.dataTable tbody th, table.dataTable tbody td{
            padding: 8px 17px;
        }
        table ul{
            list-style: none;
            padding: 0;
            list-style-type: none;
        }
        .mce-notification-warning{
            display:none;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('layouts.alerts')
                    <form method="POST" action="{{ URL('/templates/save/' . $template->id) }}">
                        {{ CSRF_FIELD() }}
                        <div class="form-group col-md-12">
                            <label class="form-label">Template Name:</label>
                            <input type="text" name="name" class="form-control" placeholder="Template Name" required="" value="{{ $template->name }}">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Craft a custom email template for a personal touch. Use the following variables for your template</label>
                            <table class="table">
                                    <thead>
                                        <tr>
                                            <th>This Person:</th>
                                            <th>Is Being Introduced To:</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><label class="label label-primary">[FIRST_NAME]</label></td>
                                            <td><label class="label label-danger">[FIRST_NAME2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[LAST_NAME]</label></td>
                                            <td><label class="label label-danger">[LAST_NAME2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[TITLE]</label></td>
                                            <td><label class="label label-danger">[TITLE2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[COMPANY]</label></td>
                                            <td><label class="label label-danger">[COMPANY2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[CELL_PHONE]</label></td>
                                            <td><label class="label label-danger">[CELL_PHONE2]</label></td>
                                        </tr>
                                        <tr>
                                            <td><label class="label label-primary">[WORK_PHONE]</label></td>
                                            <td><label class="label label-danger">[WORK_PHONE2]</label></td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Email Subject:</label>
                            <textarea id="newsubject" class="form-control" name="subject" placeholder="">{{ $template->subject }}</textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="form-label">Email Body:</label>
                            <textarea id="newtemplate" class="form-control" name="content" placeholder="Tax Attorney, Estate Planner, Family Attorney">{{ $template->content }}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="submit" name="search" class="btn btn-primary" value="Update Template">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.tiny.cloud/1/h5038yp1fjvr0a1uk6kledxt9lgvq207l43v08nqokxufexz/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        $(document).ready(function() {
            $('#connections').DataTable();

            $(".removeConnection").click(function(){
                $connectionID = $(this).attr("data-connection-id");
                $.ajax({
                  url: '/connection/remove/' + $connectionID
               });
                $(this).parent().parent().parent().parent().remove();
                $('#connections').DataTable().reload();
            });
        } );
    </script>
         <script>

  tinymce.init({
    selector: '#newtemplate',
    insert_button_items: 'image link | inserttable | textcolor',
    theme: 'modern',
    menubar: false,
  plugins: ['print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help'],
  toolbar: 'mybutton',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | mybutton',
    default_link_target: "_blank",
    style_formats: [    
        {title: 'black text', inline: 'span', styles: {color: '#000000'}},
        {title: 'black text', inline: 'p', styles: {color: '#000000'}}
    ],
    setup: function(editor) {
    editor.addButton('mybutton', {
      type: 'menubutton',
      text: 'Template Variables',
      icon: false,
      menu: [{
            text: 'Introduce Person One',
            context: 'tools',
            menu:[
                {
                    text: 'First Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[FIRST_NAME]</span>&nbsp;');
                      }
                },
                {
                    text: 'Last Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[LAST_NAME]</span>&nbsp;');
                      }
                },
                {
                    text: 'Title',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[TITLE]</span>&nbsp;');
                      }
                },
                {
                    text: 'Company',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[COMPANY]</span>&nbsp;');
                      }
                },
                {
                    text: 'Cell Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[CELL_PHONE]</span>&nbsp;');
                      }
                },
                {
                    text: 'Work Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[WORK_PHONE]</span>&nbsp;');
                      }
                }
            ]
      }, {
        text: 'To Person Two',
            context: 'tools',
            menu:[
                {
                    text: 'First Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[FIRST_NAME2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Last Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[LAST_NAME2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Title',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[TITLE2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Company',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[COMPANY2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Cell Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[CELL_PHONE2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Work Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[WORK_PHONE2]</span>&nbsp;');
                      }
                }
            ]
      }]
    });
  }
  });

  tinymce.init({
    selector: '#newsubject',
    insert_button_items: 'image link | inserttable | textcolor',
    theme: 'modern',
    menubar: false,
  plugins: ['print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help'],
  toolbar: 'mybutton',
  toolbar1: 'removeformat | mybutton',
    default_link_target: "_blank",
    style_formats: [    
        {title: 'black text', inline: 'span', styles: {color: '#000000'}},
        {title: 'black text', inline: 'p', styles: {color: '#000000'}}
    ],
    setup: function(editor) {
    editor.addButton('mybutton', {
      type: 'menubutton',
      text: 'Template Variables',
      icon: false,
      menu: [{
            text: 'Introduce Person One',
            context: 'tools',
            menu:[
                {
                    text: 'First Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[FIRST_NAME]</span>&nbsp;');
                      }
                },
                {
                    text: 'Last Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[LAST_NAME]</span>&nbsp;');
                      }
                },
                {
                    text: 'Title',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[TITLE]</span>&nbsp;');
                      }
                },
                {
                    text: 'Company',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[COMPANY]</span>&nbsp;');
                      }
                },
                {
                    text: 'Cell Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[CELL_PHONE]</span>&nbsp;');
                      }
                },
                {
                    text: 'Work Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #337ab7; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[WORK_PHONE]</span>&nbsp;');
                      }
                }
            ]
      }, {
        text: 'To Person Two',
            context: 'tools',
            menu:[
                {
                    text: 'First Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[FIRST_NAME2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Last Name',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[LAST_NAME2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Title',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[TITLE2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Company',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[COMPANY2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Cell Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[CELL_PHONE2]</span>&nbsp;');
                      }
                },
                {
                    text: 'Work Phone',
                    onclick: function() {
                        editor.insertContent('<span style="background-color: #d9534f; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em;">[WORK_PHONE2]</span>&nbsp;');
                      }
                }
            ]
      }]
    });
  }
  });
  </script>
@endsection
